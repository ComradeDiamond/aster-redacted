const dynamicCacheName = 'site-dynamic-v1';

self.addEventListener("install", function () {
    caches.open(dynamicCacheName).then(function (cache) {
        return cache.addAll([
            '/',
            '404.html'
        ]);
    })
    self.skipWaiting();
});

// activate event
self.addEventListener('activate', evt => {  //dyanmically download content for offline use as user explores the page
    evt.waitUntil(
        caches.keys().then(keys => {
            return Promise.all(keys
                .filter(key => key !== dynamicCacheName)
                .map(key => caches.delete(key))
            );
        })
    );
});

async function checkForUpdates(eventObj) {
    if (navigator.onLine && eventObj.request.url.indexOf("identitytoolkit") == -1 && eventObj.request.url.indexOf("firestore") == -1 && eventObj.request.url.indexOf("content-moderation") == -1 && eventObj.request.url.indexOf("get_standard_api_result_data") == -1 && eventObj.request.url.indexOf("gtag") == -1 && eventObj.request.url.indexOf("google-analytics") == -1) { //update content if online
        caches.open(dynamicCacheName).then(cache => {
            cache.delete(eventObj.request.url).then(function () {
                fetch(eventObj.request).then(fetchRes => {
                    cache.put(eventObj.request.url, fetchRes.clone());
                })
            }).catch(function () { });
        });
    }
}

// fetch event
self.addEventListener('fetch', evt => {
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request).then(fetchRes => {
                return fetchRes;
            }).catch(function () {
                caches.match("404.html").then(cacheRes => { //offlien
                    return cacheRes;
                }).catch(function () { })
            });
        })
    );
    evt.waitUntil(async function () {
        await evt.handled;
        return checkForUpdates(evt);
    }());
});
