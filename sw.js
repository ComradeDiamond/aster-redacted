/* dummy service worker to make website installable 
Justin Note: Smh Steven at least I don't use dark mode >:( */

self.addEventListener('install', function (e) {
	e.waitUntil(
		caches.open('cache2').then(function (cache) {
			return cache.addAll([
				'/'
			]);
		})
	);
	self.skipWaiting();
});

async function sendResponse(eventObj) {
	//Chrome will throw smth like TypeError: Failed to execute 'fetch' on 'WorkerGlobalScope': 'only-if-cached' can be set only with 'same-origin' mode
	//But the if (e.req.cache) thing is a fix to the chromium bug because welp, chrome sucks
	caches.match(eventObj.request).then(cacheRes => {
		if (eventObj.request.cache == 'only-if-cached' && eventObj.request.mode != 'same-origin') {
			return;
		}

		return cacheRes || fetch(eventObj.request).then(fetchRes => {
			return fetchRes;
		});
	});
}

async function checkForUpdates() {
	caches.open('cache2').then(function (cache) {
		cache.delete("/");
		cache.addAll([
			'/'
		]);
	});
}

self.addEventListener('fetch', evt => {
	evt.waitUntil(async function () {
		await sendResponse(evt);
		await evt.handled;
		return checkForUpdates();
	}());
});