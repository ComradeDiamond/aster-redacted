// file system module to perform file operations
const fs = require('fs');

// json
var json = eval(fs.readFileSync('./assets/js/clubs.js') + '')

for (var i = 0; i < json.length; i++) {

    // parse json -> string
    var json_obj = JSON.parse(JSON.stringify(json));
    console.log(json_obj);

    name = json_obj[i].name;
    category = json_obj[i].category;
    school = json_obj[i].school;
    meets = json_obj[i].meets;
    room = json_obj[i].room;
    time = json_obj[i].time;
    link = json_obj[i].link;

    // stringify JSON Object
    var clubContent = "---\nlayout: info\nname: " + name + "\ncategory: " + category + "\nschool: " + school +
        "\nmeets: " + meets + "\nroom: " + room + "\ntime: " + time + "\npermalink: " + link + "\n---\nsome content";
    console.log(clubContent);

    fs.writeFile("./_clubs/" + name + ".md", clubContent, 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to markdown.");
            return console.log(err);
        }

        console.log("Markdown file has been saved.");
    });
}
