$(".filter_link").on('click', function () {
    $(".filter_menu").toggleClass('toggleDisplay');

    var filterLinkElement = document.getElementsByClassName("filter_link")[0];

    if ($(".filter_menu").hasClass('toggleDisplay')) {

        var close_menu = '<i class="material-icons align-icons">cancel</i> Hide Filters';
        filterLinkElement.innerHTML = close_menu;
        filterLinkElement.style.background = "var(--primary-color)";
        filterLinkElement.style.color = "var(--color-white)";
    }

    else {
        var open_menu = '<i class="material-icons align-icons">sort</i> Advanced Filters';
        filterLinkElement.innerHTML = open_menu;
        filterLinkElement.style.background = "";
        filterLinkElement.style.color = "";
    }
});

let renderSearch = new readClubs;
class Filter {
    byName(input) {

        let club_name = input.toLowerCase();

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        function search(lastIndex, limit) {
            firestore.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    if (!indexesRendered.includes(doc.id)) {
                        indexesRendered.push(doc.id);
                        console.log(doc.id, " => ", doc.data().keywords);
                        renderSearch.render("info", "searchResults", doc, doc.data().school, "Schools");
                        addEditEvents(doc.id, "Schools");
                    }
                });
            });


            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;

        }

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    byCategory(array) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        function search(lastIndex, limit) {
            firestore.collection("Schools").where('category', 'array-contains-any', array).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    if (!indexesRendered.includes(doc.id)) {
                        indexesRendered.push(doc.id);
                        console.log(doc.id, " => ", doc.data().keywords);
                        renderSearch.render("info", "searchResults", doc, doc.data().school, "Schools");
                        addEditEvents(doc.id, "Schools");
                    }
                });
            });

            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }
        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    byNameAndCategory(name, category_array) {

        let dataTransport = {};
        let lastDoc;
        let club_name = name.toLowerCase();
        let indexesRendered = [];
        function search(lastIndex, limit) {
            firestore.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {

                doc.forEach(function (snapshot) {
                    category_array.forEach((category) => {
                        if (snapshot.data().category.includes(category) && !indexesRendered.includes(snapshot.id)) {
                            indexesRendered.push(snapshot.id);
                            renderSearch.render("info", "searchResults", snapshot, snapshot.data().school);
                            addEditEvents(snapshot.id, "Schools");
                        }
                    });
                })

            });
            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }
        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    bySchool(school_array) {

        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];
        function search(lastIndex, limit) {
            school_array.forEach((school) => {
                firestore.collection("Schools").where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        if (!indexesRendered.includes(doc.id)) {
                            indexesRendered.push(doc.id);
                            renderSearch.render("info", "searchResults", doc, doc.data().school, "Schools");
                            addEditEvents(doc.id, "Schools");
                        }
                    });
                });
            });
            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    bySchoolAndName(school_array, name) {

        const club_name = name.toLowerCase();
        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        function search(lastIndex, limit) {
            school_array.forEach((school) => {
                firestore.collection("Schools").where('school', '==', school).where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {
                    doc.forEach(function (snapDoc) {
                        if (!indexesRendered.includes(snapDoc.id)) {
                            indexesRendered.push(snapDoc.id);
                            renderSearch.render("info", "searchResults", snapDoc, snapDoc.data().school, "Schools");
                            addEditEvents(snapDoc.id, "Schools");
                        }
                    });
                });

            });
            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    bySchoolAndCategory(school_array, category_array) {
        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];

        function search(lastIndex, limit) {

            school_array.forEach((school) => {
                firestore.collection("Schools").where('category', 'array-contains-any', category_array).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {

                    doc.forEach(function (snapDoc) {
                        if (!indexesRendered.includes(snapDoc.id)) {
                            indexesRendered.push(snapDoc.id);
                            renderSearch.render("info", "searchResults", snapDoc, snapdoc.data().school, "Schools");
                            addEditEvents(snapDoc.id, "Schools");
                        }

                    });
                });

            });
            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    bySchoolNameCategory(school_array, category_array, name) {

        const club = name.toLowerCase();
        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];

        function search(lastIndex, limit) {
            school_array.forEach((school) => {
                firestore.collection("Schools").where('keywords', 'array-contains-any', [club]).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        category_array.forEach((category) => {
                            if (doc.data().category.includes(category) && !indexesRendered.includes(doc.id)) {
                                indexesRendered.push(doc.id);
                                renderSearch.render("info", "searchResults", doc, doc.data().school, "Schools", "Schools");
                                addEditEvents(doc.id, "Schools");
                            }
                        })
                    })
                });
            })


            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            dataTransport.lastIndex = lastIndex;
            dataTransport.lastIndex = parseFloat(dataTransport.lastIndex) + 1;
        }

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        window.addEventListener("scroll", helper); //add new
        document.getElementsByClassName("applyButton")[0].handler = helper;

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


}

let lazy = new makeItLazy;
function submited() {
    lazy.lazy("submission", "subclubs", "Submissions");
}
function kickoff() {
    var toRemove = document.getElementsByClassName("applyButton").handler; //obtain the reference to old
    if (toRemove != undefined) { //prevents error from removing undefined
        window.removeEventListener("scroll", toRemove); //remove old
    }
    indexTransport = {};
    indexesRendered = [];
    var container = document.createElement('div');
    container.id = "container";
    $("#browse").append(container);
    lazy.lazy("info", "container", "Schools");

    //submissions
    submited();
}

function searchData() {

    let $categories = [];
    let $schools = [];
    $("#searchResults").remove();
    $("#container").remove();
    let search = new Filter;
    let search_value = $("#search").val();

    var results = document.createElement('div');
    results.id = "searchResults";
    $("#browse").append(results);

    let $categoriesSelected = $('input[name="category[]"]:checked');
    let $schoolsSelected = $('input[name="school[]"]:checked');

    if ($schoolsSelected.length > 0) {
        $schoolsSelected.each(function () {
            $schools.push($(this).val());
        });
    }
    else {
        $schools = [];
    }


    if ($categoriesSelected.length > 0) {
        $categoriesSelected.each(function () {
            $categories.push($(this).val());
        });

    }
    else {
        $categories = [];
    }


    // check which cases to execute
    //default
    if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
        var toRemove = document.getElementsByClassName("applyButton").handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }
        indexTransport = {};
        indexesRendered = [];
        var container = document.createElement('div');
        container.id = "container";
        $("#browse").append(container);
        lazy.lazy("info", "container", "Schools");
    }

    // by name only
    if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
        search.byName(search_value);
    }

    //category only
    if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
        search.byCategory($categories);
    }

    // school only
    if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
        search.bySchool($schools);
    }

    //category and name

    if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
        search.byNameAndCategory(search_value, $categories);
    }

    // category and school

    if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
        search.bySchoolAndCategory($schools, $categories);
    }

    // by school and name
    if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
        search.bySchoolAndName($schools, search_value);
    }

    // by school, name, and category
    if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
        search.bySchoolNameCategory($schools, $categories, search_value)
    }

}

$(document).ready(function () {
    $(".applyButton").on('click', function () {
        searchData();
    });
    document.getElementById("search").addEventListener("keydown", function (event) {
        if (event.key == "Enter") {
            searchData();
        }
    });
});


document.addEventListener("DOMContentLoaded", function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            kickoff();
        }
    });
});