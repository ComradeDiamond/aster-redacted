
document.addEventListener("DOMContentLoaded", function () {
    coldVisit = true;
    firebase.auth().onAuthStateChanged(function (user) {
        if (user && coldVisit) {
            location.href = '/dashboard';
        }
    });

    //init selectTab to the tab which the user clicked for. defaulted to login, but if we are going to add a button that redirects straight to signup, then selectTab that instead
    selectTab(document.getElementById("logintab"));

    for (var el of document.getElementsByClassName("entry")) {
        mdc.textField.MDCTextField.attachTo(el);
    }

    document.getElementById("toggle-password").onclick = function () { togglePass("password", this); }
    document.getElementById("toggle-new-password").onclick = function () { togglePass("new-password", this); }

    function togglePass(fieldid, eye) {
        if (![...eye.classList].includes("fa-eye-slash")) {
            document.getElementById(fieldid).type = "text";
            return eye.classList.add("fa-eye-slash");
        }
        document.getElementById(fieldid).type = "password";
        eye.classList.remove("fa-eye-slash");
    }
});
function selectTab(el) {
    for (let item of document.getElementsByClassName("mdc-tab")) {
        item.classList.remove("mdc-tab--active");

        let underline = item.querySelector(".mdc-tab-indicator");
        underline.classList.remove("mdc-tab-indicator--active");

        let tab = document.getElementById(item.id.slice(0, -3));
        tab.style.visibility = "hidden";
        tab.style.height = "0px";
        tab.style.marginTop = "0";
        let arr = tab.getElementsByTagName("input");
        for (let i = 0; i < arr.length; i++) {
            arr[i].required = false;
        }
    }
    
    el.classList.add("mdc-tab--active");
    let underline = el.querySelector(".mdc-tab-indicator");
    underline.classList.add("mdc-tab-indicator--active");
    let tab = document.getElementById(el.id.slice(0, -3));
    tab.style = "initial";
    let arr = tab.getElementsByTagName("input");
    for (let i = 0; i < arr.length; i++) {
        arr[i].required = true;
    }
}
function capitalize(str) {
    return str.substr(0, 1).toUpperCase() + str.substr(1, str.length - 1);
}

function login() {
    if (!navigator.onLine) return mdcWarn("You cannot log in when you are offline");
    if (firebase.auth().currentUser) return mdcWarn("You are already signed in"); //redundant

    var userEmail = document.getElementById("email").value,
        userPass = document.getElementById("password").value;

    //replace this with an indication of the specific box in the future.
    if (!userEmail || !userPass) return console.log("You have not filled in all the required information");
    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(userEmail))) return mdcWarn("Invalid email");
    firebase.auth().signInWithEmailAndPassword(userEmail, userPass).then(function () {
        coldVisit = false;
        history.back();
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;

        switch (errorCode) {
            case 'auth/wrong-password':
                return mdcWarn("Incorrect password");
            case 'auth/user-not-found':
                return mdcWarn("User is not found. Please double-check your spelling");
        }
    });
}
function signup() {
    if (!navigator.onLine) return mdcWarn("You cannot log in when you are offline");
    if (firebase.auth().currentUser) return mdcWarn("You are already signed in"); //redundant

    var firstName = DOMPurify.sanitize(document.getElementById("new-name1").value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        lastName = DOMPurify.sanitize(document.getElementById("new-name2").value, { ALLOWED_TAGS: [], KEEP_CONTENT: true });
    userEmail = document.getElementById("new-email").value,
        userName = DOMPurify.sanitize(document.getElementById("new-username").value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        userPass = document.getElementById("new-password").value,
        userConfirmPass = document.getElementById("confirm-new-password").value;

    //replace this with an indication of the specific box in the future.
    if (!firstName || !lastName || !userEmail || !userPass || !userName) return console.log("You have not filled in all the required information");
    if (!(/^[a-zA-Z]+$/.test(firstName)) || !(/^[a-zA-Z]+$/.test(lastName))) return mdcWarn("Your first and last name may only include letters");
    if (!(/^[a-zA-Z0-9-_].{3,12}$/.test(userName))) return mdcWarn("Your username must be between 3-12 characters and may only contain letters, numbers, dashes and underscores");
    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(userEmail))) return mdcWarn("You have entered an invalid email");
    if (!(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,32}$/.test(userPass))) return mdcWarn("Your password must be 6 characters long and contain at least:\nOne uppercase letter\nOne lowercase letter\nOne number");
    if (userPass !== userConfirmPass) return mdcWarn("Your passwords do not match");

    firebase.auth().createUserWithEmailAndPassword(userEmail, userPass).then(function () {
        var user = firebase.auth().currentUser;
        if (firebase.auth().currentUser) {
            var userRef = firebase.database().ref('users/');
            userRef.child(user.uid).set({
                firstname: capitalize(firstName),
                lastname: capitalize(lastName),
                username: userName,
                role: "standard",
            });
            //upload local club interest to firestore
            var userRef = firebase.database().ref('/users/').child(firebase.auth().currentUser.uid);
            userRef.once("value", function () {
                userRef.update({
                    interestedClubs: localStorage.interestedClubs
                });
            });

            // reauth
            var credential = firebase.auth.EmailAuthProvider.credential(userEmail, userPass);
            user.reauthenticateWithCredential(credential).then(() => {
                console.log("reauth was a success!")
            });
            coldVisit = false;
            user.sendEmailVerification().then(function () {
                console.log("email sent");
                mdcWarn("Sign in again after verification for changes to take effect.");
                history.back();
            }).catch(function (error) {
                // An error happened.
            });
        }
    }).catch(function (error) {
        if (error) return mdcWarn(error.message);
    });
}
function logout() {
    firebase.auth().signOut();
}