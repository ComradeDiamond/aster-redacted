imgShuttle = {};
document.addEventListener("DOMContentLoaded", function(){
    // pfp image
    var pfp_image = document.getElementById('profile_img');
    if (navigator.onLine) {
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                for (var el of document.getElementsByClassName("accountBtn")) {
                    el.style.display = "";
                }
                var userRef = firebase.database().ref('/users/').child(user.uid);
                
                //drawer email
                document.getElementById("drawerEmail").innerHTML = user.email;
                pfp_image.className = "profile_comment";

                const defaultImg = "/assets/images/default.PNG";

                var userDiv = document.createElement("div");
                userDiv.id = "user_div";

                var dropDownIcon = document.createElement("i");
                dropDownIcon.className = "material-icons accented-icons nav_icon";
                dropDownIcon.id = "pfMenuDropDown";
                dropDownIcon.innerHTML = "keyboard_arrow_down";
                dropDownIcon.style.cursor = "pointer";
                // navbar pfp
                var pfpNav = document.createElement("img");
                pfpNav.className = "nav_pfp";

                
                var userBtn = document.getElementById("userBtn");
                $("#userBtn").empty();

                firecloud.ref().child("pfp/" + user.uid).getDownloadURL()
                .then((url) => {
                    pfp_image.src = url;
                    pfpNav.src = url;
                    imgShuttle.image = url;
                })
                .catch(() => {
                    pfp_image.src = defaultImg;
                    pfpNav.src = defaultImg;
                    imgShuttle.image = defaultImg; 
                });
        
                userRef.on("value", function(snapshot){
                        
                    userBtn.append(pfpNav);
                    userBtn.append(dropDownIcon)
                    imgShuttle.name = DOMPurify.sanitize(snapshot.val().username, {ALLOWED_TAGS: [], KEEP_CONTENT: true});

                    document.getElementById("drawerUsername").innerHTML = DOMPurify.sanitize(snapshot.val().username, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
                    document.getElementById("ibc").style.display = "";

                    if(snapshot.val().role !== "manager") {
                        document.getElementById("managerLink").style.display = "none";
                        document.getElementById("manageBtn").style.display = "none";
                    }
                    loadOut();
                });
                document.getElementById("loginBtn").style.display = "none";

                document.getElementById("userBtn").addEventListener("click", function(e) {
                    e.preventDefault();
                    toggleDropDown(e);
                });
            } else {
                pfp_image.remove();
                loadOut();
            }
        });
    } else {
        loadOut();
    }
});
function logOut(e){
    e.preventDefault();
    firebase.auth().signOut();
    if(window.location.pathname == "/dashboard"){
        location.href = "/login";
    }
    else{
        location.reload();
    }
}
function loadOut(){
    document.getElementById("loading-screen").classList.add("fade");
    setTimeout(function(){
        document.getElementById("loading-screen").style.display = "none";
        document.getElementById("loading-flower").classList.remove("loading-spin");
    }, 500);
}
function toggleDropDown(e){
    var menu = document.getElementById("dropdownMenu");
    var dropdown = document.getElementById("pfMenuDropDown");
    if(menu.style.display == "none" && e.target !== document.getElementById("dropdownMenu")){
        dropdown.classList.add("pfdropDownFlip")
        return menu.style.display = "block";
    }
    
    dropdown.classList.remove("pfdropDownFlip");
    menu.style.display = "none";
}

/*
window.addEventListener("error", (e) => {
    loadOut();
}, true);
window.addEventListener("click", (e) => {
    if(e.target !== document.getElementById("dropdownMenu"))
    toggleDropDown(e);
});
*/