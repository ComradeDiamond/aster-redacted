function init() {
    for (var el of document.getElementsByClassName("entry")) {
        mdc.textField.MDCTextField.attachTo(el);
    }
}
document.addEventListener("DOMContentLoaded", function () {

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            location.href = '/dashboard'
        }
        else {
            function resetEmail() {
                var auth = firebase.auth();
                var emailAddress = $("#reset").val();

                auth.sendPasswordResetEmail(emailAddress).then(function () {
                    mdcWarn("Check your email for instructions");
                    location.href = "login";
                }).catch(function (error) {
                    console.log(error)
                });
            }

            $("#send").click(function () {

                resetEmail();
            });
        }
    });
}) 
