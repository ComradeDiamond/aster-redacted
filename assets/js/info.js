function init() {
    wrkDivJS = document.getElementById("wrkDiv");
    slideUl = document.getElementById("slideUlHTML");
    //init material design textboxes
    for (var el of document.getElementsByClassName("entry")) {
        mdc.textField.MDCTextField.attachTo(el);
    }
}
function splitArray(str) {
    const arr = [];
    let array = str.replace(/\s/g, "").split(',');
    array.forEach((word) => {
        arr.push(word);
    })
    return arr
}

//Get Image for slideshow
function getImage(relPath, slideImg) {
    try {
        var sRef = firecloud.ref().child(relPath).getDownloadURL().then((url) => {
            slideImg.src = url;
        })
    }
    catch (err) {
    }
}

//Jekyll workaround- uses the items inside the image .md file variable to loop and create all slideshow images
function slideCreate(wrkAroundDiv) {
    //The split item could change if we need -- if there is no image, screw the thing
    try
    {
        let l = wrkAroundDiv.innerText.split(";");
    }
    catch
    {
        return;
    }

    for (var i in l) {
        let slideItem = document.createElement("li");
        slideItem.setAttribute("data-uk-slideshow-item", i.toString());

        let slideImg = document.createElement("img");
        slideImg.setAttribute("uk-cover", "");

        //Fetch image
        var result = getImage(l[i], slideImg);

        slideItem.appendChild(slideImg);
        slideUl.appendChild(slideItem);
    }
    wrkAroundDiv.remove();
}

function displayNews(elemId, date, title, description) {
    let dateEl = document.createElement('h1');
    let titleEl = document.createElement('h3');
    let descriptionEl = document.createElement('p');
   
    titleEl.innerHTML = title;
    document.getElementById(elemId).append(titleEl);

    dateEl.className = 'title-up';
    dateEl.innerHTML = date;
    document.getElementById(elemId).append(dateEl);
    
    descriptionEl.innerHTML = description;
    document.getElementById(elemId).append(descriptionEl);
}


function getPosts() {
    const club_name = shuttle.club_name;
    const club_school = shuttle.club_school;
    let posts_feed = database.ref('club_posts/').child(club_school);
    posts_feed.on("child_added", function(admin){
        posts_feed.child(admin.key).child(club_name).on('child_added', function(club){
            displayNews("announcements", club.key, club.val().title, club.val().desc);
        })
    });
}
document.addEventListener("DOMContentLoaded", function () {
    getPosts()
});