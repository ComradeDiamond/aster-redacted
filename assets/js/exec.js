function init(){
    selectTab(document.getElementById("newposttab"));
    for (var el of document.getElementsByClassName("entry")) {
        mdc.textField.MDCTextField.attachTo(el);
    }
}
function selectTab(el) {
    for (let item of document.getElementsByClassName("dash-link")) {
      item.classList.remove("active");
      var tab = document.getElementById(item.id.slice(0, -3));
      tab.style.display = "none";
    }
  
    el.classList.add("active");
    document.getElementById(el.id.slice(0, -3)).style.display = "block";
    var arr = document
      .getElementById(el.id.slice(0, -3))
      .getElementsByTagName("input");
}

function displayNews(elemId, date, title, description) {
    let dateEl = document.createElement('h1');
    let titleEl = document.createElement('h3');
    let descriptionEl = document.createElement('p');
   
    titleEl.innerHTML = title;
    document.getElementById(elemId).append(titleEl);

    dateEl.className = 'title-up';
    dateEl.innerHTML = date;
    document.getElementById(elemId).append(dateEl);
    
    descriptionEl.innerHTML = description;
    document.getElementById(elemId).append(descriptionEl);
}


function getAnnounceBy(user_id){
    posts = database.ref('club_posts/');
    posts.on('child_added', function(school){ 
        var newRef = posts.child(school.key).child(user_id);
        newRef.on("child_added", function(club){
            newRef.child(club.key).on("child_added", function(info){

                displayNews("announces", info.val().title, info.key, info.val().desc);
            });
        });
    });
}

document.addEventListener("DOMContentLoaded", function () {
    getAnnounceBy(firebase.auth().currentUser.uid);        
});