/**
 * This callback initializes the interested clubs array, then kicks off loading of interest related content
 */
document.addEventListener("DOMContentLoaded", function() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) { //with account
            var userRef = firebase.database().ref('/users/').child(user.uid);
            userRef.once("value", function(snapshot){
                if(snapshot.hasChild("interestedClubs")) {
                    interestedClubs = JSON.parse(snapshot.val().interestedClubs);
                } else {
                    interestedClubs = [];
                }
                kickoff();
            });

        } else { //no account
            if (localStorage.interestedClubs == undefined) {
                localStorage.interestedClubs = "[]";
            }
            interestedClubs = JSON.parse(localStorage.interestedClubs);
            kickoff();
        }
    })
});

/**
 * This function changes input string to N/A if empty
 */

function checkEmpty(first, second) {
	let str;
    if(first.replace(/\s/g, "").length == 0 || second.replace(/\s/g, "").length == 0 ) {
        str = "N/A"
    }
	else {
		str = first + " " + second;
	}
    return str
}

/**
 * This function adds a space after each comma in the category section
 */
function spaces(category_array) {
  const arr = [];
  category_array.forEach((category) => {
    arr.push(" " + category)
  });
  return arr;

}

/**
 * 
 * @param {String} elementId ID of container element to append card to
 * @param {Object} club JSON of club containing the data to populate in its card
 * 
 * This function takes information from club, turns it into an HTML card and append it to the container with id of elementId
 */
function cardDisplay(elementId, club, school) {
    //data
    var name = DOMPurify.sanitize(club.data().name, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
    var category = spaces(club.data().category);
    var link = club.data().link;
    var meets = DOMPurify.sanitize(club.data().meets, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
    var room = DOMPurify.sanitize(club.data().room, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
    var time = DOMPurify.sanitize(club.data().time, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
    var description = DOMPurify.sanitize(club.data().description, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
    var element = document.getElementById(elementId);

    //card frame
    var mainElement = document.createElement("li");
    var cardElement = document.createElement("div");
    cardElement.className = "sul-box-raised-3 with-hover";
    mainElement.appendChild(cardElement);

    //interest button
    var interestButtonElement = document.createElement("button");
    var playlistBtn = document.createElement("i");
    playlistBtn.className = "material-icons";
    playlistBtn.innerHTML = "playlist_add";
    interestButtonElement.className = "cardBtn";
    interestButtonElement.append(playlistBtn);
    cardElement.appendChild(interestButtonElement);

    playlistBtn.club = new Club(name, school, playlistBtn);
    if (playlistBtn.club.interest()) {
        playlistBtn.innerHTML = "playlist_add_check";
    }
    playlistBtn.addEventListener("click", function () {
        this.club.updateInterest();
    });

    //card contents
    var detailsElement = document.createElement("div");
    detailsElement.className = "card-details";
    cardElement.appendChild(detailsElement);

    //details children
    var nameElement = document.createElement("div");
    var tagsElement = document.createElement("div");
    var aboutElement = document.createElement("div");
    var skillsElement = document.createElement("div");
    var descriptionElement = document.createElement("div");
    nameElement.className = "name";
    tagsElement.className = "tags";
    aboutElement.className = "card-about";
    skillsElement.className = "skills";
    descriptionElement.className = "description";
    descriptionElement.innerHTML = description;//.slice(0, 150) + "...";
    nameElement.innerHTML = name;
    tagsElement.innerHTML = category;
    
    
    //card about children
    var meetDayElement = document.createElement("div");
    meetDayElement.className = "item";

    var meetElement = document.createElement("h5");
    meetElement.className = "tags day";
    meetElement.innerHTML = "Meets";

    meetDayElement.append(meetElement);

    var dayElement = document.createElement("div");
    dayElement.className = "time";
    dayElement.innerHTML = checkEmpty(meets, time);
    meetDayElement.append(dayElement);

    aboutElement.append(meetDayElement);
 
    //school
    var schoolElement = document.createElement("span");
    schoolElement.className = "school";
    schoolElement.innerHTML = school;
   
    detailsElement.appendChild(schoolElement);
    detailsElement.appendChild(nameElement);
    detailsElement.appendChild(tagsElement);
    detailsElement.appendChild(aboutElement);
    detailsElement.appendChild(descriptionElement);
    detailsElement.appendChild(skillsElement);


    //skills children
    var readMoreElement = document.createElement("div");
    readMoreElement.className = "readMore";
    skillsElement.appendChild(readMoreElement);

    //readmore children
    var buttonElement = document.createElement("span");
    buttonElement.className = "sul-btn btn-primary tags";
    buttonElement.onclick = () => location.href = link;
    
    //anchor link
    var anchorElement = document.createElement("a");
    anchorElement.href = link;
    anchorElement.innerHTML = "Read More"
    buttonElement.appendChild(anchorElement);
    
    readMoreElement.appendChild(buttonElement);

    element.appendChild(mainElement);
}


//club class
/**
 * 
 * @param {String} name Name of the club
 * @param {String} school Name of the school the club belongs to
 * @param {HTMLButtonElement} button The button the user will click on to update interest
 */
function Club(name, school, button) {
    this.name = name;
    this.school = school;
    this.button = button;
}

/**
 * This function toggles the interest of the user in a club
 */
Club.prototype.updateInterest = function () {
    let btnHtml = this.button;
    let school = this.school;
    let name = this.name;

    var storedName = school + "/" + name;
    if(this.interest()){
        btnHtml.innerHTML = 'playlist_add';

        var idx = interestedClubs.indexOf(this.school + "/" + this.name);
        interestedClubs.splice(idx, 1);
    } else {
        btnHtml.innerHTML = 'playlist_add_check';
        interestedClubs.push(storedName);
    }
    interestSync();
}

/**
 * This method checks the interestedClubs global to determine if the user is interested in the club
 * 
 * @returns {Boolean} Whether this is in interestedClubs
 */
Club.prototype.interest = function () {
    let school = this.school;
    let name = this.name;
    var storedName = school + "/" + name;
    return (interestedClubs.indexOf(storedName) > -1);
}

/**
 * This function syncs the interest list to firebase if logged in, and to localstorage if not
 */
function interestSync() {
    var interestString = JSON.stringify(interestedClubs);
    if (firebase.auth().currentUser) { //account
        var userRef = firebase.database().ref('/users/').child(firebase.auth().currentUser.uid);
        userRef.once("value", function(){
            userRef.update({
                interestedClubs: interestString
            });
        });
    } else { //no account
        localStorage.interestedClubs = interestString;
    }
}
