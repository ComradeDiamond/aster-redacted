//Requires firebase_base.html
const defaultImg = "/assets/images/default.PNG";

//Easier way to get the pfp link - the proper way would be to search it up in realtime database
function getPfpLink() {
    var userId = firebase.auth().currentUser.uid;

    try
    {
        firecloud.ref().child("pfp/" + userId).getDownloadURL()
            .then((url) => {
                return url;
            })
    }
    catch
    {
        return defaultImg;
    }
}

//Local Storage, if we decide to store userPfpImg clientside so we don't have tons of server reads
//Only and must be executed once
async function storeLocal(){
    var link = await getPfpLink();
    try {
        localStorage.asterPfpLink = link;
    }
    catch(err)
    {
        console.log("Failure to set local PFP Link");
    }
}

//These two stores the PFP image in whatever database info column you decide to put it in.
//Ie. you can use thse to store PFP images inside the forum.
//Now, you can retrieve the download link without having to send a seperate read request to firebase storage

//Realtime Database
async function storePfpRT(path, fromLocal) {
    if ((!fromLocal) || (fromLocal == undefined))
    {
        var link = await getPfpLink();
    }
    else
    {
        try
        {
            var link = localStorage.asterPfpLink;
        }
        catch(err)
        {
            console.log("Error fetching local storage link");
            console.log(err);
        }
    }
    database.ref(path).update({
        pfpImg: link
    }).catch((err) => {
        console.log(err);
    })
}

//Firestore
async function storePfpFS(collection, document, fromLocal) {
    if ((!fromLocal) || (fromLocal == undefined))
    {
        var link = await getPfpLink();
    }
    else
    {
        try
        {
            var link = localStorage.asterPfpLink;
        }
        catch(err)
        {
            console.log("Error fetching local storage link");
            console.log(err);
        }
    }
    database.collection(collection).doc(document).update({
        pfpImg: link
    }).catch((err) => {
        console.log(err);
    })
}