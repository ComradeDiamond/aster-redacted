function selectTab(el) {
    for (let item of document.getElementsByClassName("dash-link")) {
        window.scrollTo(0, 0);
        item.classList.remove("active");
        var tab = document.getElementById(item.id.slice(0, -3));
        tab.style.display = "none";
    }

    el.classList.add("active");
    document.getElementById(el.id.slice(0, -3)).style.display = "block";
}

function addStatusEvents(clubId, collection) {
    let id = "#" + clubId + collection;
    let infoShuttle = {};
    var userRef = firebase.database().ref('/users/').child(firebase.auth().currentUser.uid);
    userRef.on("value", function (snapshot) {
        infoShuttle.username = snapshot.val().username.toString();
    });
    var modal = document.getElementById("thankModal");
    $(id + "_approve").on('click', function () {
        document.getElementById("usernamefield").value = "";
        document.getElementById("changeType").innerHTML = "Approve?"
        modal.style.display = "flex";
        let username = infoShuttle.username;

        $(".close").on('click', function () {
            let entered_name = document.getElementById("usernamefield").value;
            if (entered_name === username) {
                modal.style.display = "none";
                approve(clubId, collection)
            }

            else {
                console.log("Username was: " + username);
                console.log("Entered username was: " + entered_name);

                mdcWarn("no dice, sorry")
            }


        });
        console.log("approve clicked");
    });

    $(id + "_unapprove").on('click', function () {
        document.getElementById("usernamefield").value = "";
        document.getElementById("changeType").innerHTML = "Unapprove?";
        modal.style.display = "flex";
        let username = infoShuttle.username;

        $(".close").on('click', function () {
            let entered_name = document.getElementById("usernamefield").value;
            if (entered_name === username) {
                modal.style.display = "none";
                unapprove(clubId, collection, "Schools")
            }

            else {
                console.log("Username was: " + username);
                console.log("Entered username was: " + entered_name);

                mdcWarn("no dice, sorry")
            }


        });
        console.log("approve clicked");
    });
}

function approve(clubId, collection) {
    let increment = firebase.firestore.FieldValue.increment(1);
    firestore.collection("Schools").doc("counter").get().then(function (snap) {
        firestore.collection("Schools").doc("counter").update({
            count: increment
        })
        let index = snap.data().count.toString();

        firestore.collection(collection).doc(clubId).get().then(function (doc) {
            firestore.collection("Schools").doc(index).set({
                time: DOMPurify.sanitize(doc.data().time, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                description: DOMPurify.sanitize(doc.data().description, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                name: DOMPurify.sanitize(doc.data().name, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                room: DOMPurify.sanitize(doc.data().room, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                meets: DOMPurify.sanitize(doc.data().meets, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                keywords: doc.data().keywords,
                category: doc.data().category,
                school: DOMPurify.sanitize(doc.data().school, { ALLOWED_TAGS: [], KEEP_CONTENT: true })
            })
        }).then(() =>
            unapprove(clubId, collection)
        )
    })
}
/** this function generates keywords for users to search  the database by */
function generateKeywords(str) {
    const arr = [];
    let strArr = str.split('');
    let currentStr = '';
    strArr.forEach((word) => {
        currentStr += word;
        arr.push(currentStr)
    });
    str.split(' ').forEach(word => {
        arr.push(word);
    })
    var unique = arr.filter(function (itm, i, arr) {
        return i == arr.indexOf(itm);
    });
    return unique;
}

function addToCollection(formId, dest) {
    let form = document.getElementById(formId);
    if (!form.checkValidity()) {
        console.log("not valid");
        return;
    }

    //If username doesn't exist as admin in collections, don't allow them to add to collection and instead, warn modal
    let authorizeAdmin = () => new Promise((resolve, reject) => {
        var user = firebase.auth().currentUser;
        var get = firestore.collection("managers").doc(user.uid).get();
    
        get.then(doc => {
            if (doc != undefined && doc.data().isAdmin)
            {
                resolve();
            }
            else
            {
                reject();
            }
        });

        get.catch(err => {
            reject();
        })
    });

    var dataAuth = authorizeAdmin();

    dataAuth.then(() => {
        /* Not so sure about this because it's very buggy, so I'll write another one
        So for now it's commented out
        let increment = firebase.firestore.FieldValue.increment(1);

        firestore.collection(dest).doc("counter").get().then(function (snap) {
            firestore.collection(dest).doc("counter").update({
                count: increment
            })
            let index = snap.data().count.toString();

            var categories = [];
            let $categoriesSelected = $('input[name="addCat[]"]:checked');

            $categoriesSelected.each(function () {
                categories.push($(this).val());
            });


            firestore.collection(dest).doc(index).set({

                time: DOMPurify.sanitize(form.time.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                description: DOMPurify.sanitize(form.message.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                name: DOMPurify.sanitize(form.clubname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                room: DOMPurify.sanitize(form.rm.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                meets: DOMPurify.sanitize(form.meet.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                keywords: generateKeywords(DOMPurify.sanitize(form.clubname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true })),
                category: categories,
                school: DOMPurify.sanitize(form.schoolname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true })
            })
        })*/

        firestore.collection("Counters").doc("0").get()
            .then(snap => {
                let increment = +snap.data().clubs + 1;
                let index = increment + "";

                firestore.collection("Counters").doc("0").update({
                    clubs: index
                });

                var categories = [];
                let $categoriesSelected = $('input[name="addCat[]"]:checked');

                $categoriesSelected.each(function () {
                    categories.push($(this).val());
                });


                firestore.collection(dest).doc(index).set({
                    time: DOMPurify.sanitize(form.time.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                    description: DOMPurify.sanitize(form.message.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                    name: DOMPurify.sanitize(form.clubname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                    room: DOMPurify.sanitize(form.rm.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                    meets: DOMPurify.sanitize(form.meet.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
                    keywords: generateKeywords(DOMPurify.sanitize(form.clubname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true })),
                    category: categories,
                    school: DOMPurify.sanitize(form.schoolname.value, { ALLOWED_TAGS: [], KEEP_CONTENT: true })
                });

                //Clears form
                form.time.value = "";
                form.message.value = "";
                form.clubname.value = "";
                form.rm.value = "";
                form.meet.value = "";
                form.schoolname.value = "";
                
                Array.from(form.getElementsByClassName("cat")).forEach(div => {
                    div.children[0].checked = false;
                });
            })
    });

    dataAuth.catch(() => {
        //requires jekyll to import head.html
        mdcWarn("You're missing admin permisions. Please contact someone to edit this form.");
    });
}

function unapprove(clubId, collection) {
    let id = clubId + collection;
    console.log(clubId);
    console.log(id);
    firestore.collection(collection).doc(clubId).delete().then(function () {
        console.log("Document successfully deleted!");
    }).catch(function (error) {
        console.error("Error removing document: ", error);
    });
    $(`[id='${id}']`).hide();
}

function addEditEvents(clubId, collection) {
    let id = "#" + clubId + collection;
    $(id + "_edit").on('click', function () {
        editClubClicked(clubId, collection);
        console.log("edit clicked");
    });

    $(id + "_delete").on('click', function () {
        deleteClubClicked(clubId, collection);
        console.log("delete clicked");
    });
}

/**
 * db.collection("Schools").get().then(function(querySnapshot){
        querySnapshot.forEach(function(doc) {
            firestore.collection("Schools").doc(doc.id).set({
                time: doc.data().time,
                description: doc.data().description,
                name: doc.data().name,
                room: doc.data().room,
                meets: doc.data().meets,
                keywords: doc.data().keywords,
                category: doc.data().category,
                school: doc.data().school
        });
        });
    });
    update script

    realtime updates
    
// REALtime updates 
    db.collection("blog").where(firebase.firestore.FieldPath.documentId(), "==", "0")
        .onSnapshot(function(snapshot) {
            snapshot.docChanges().forEach(function(change) {
                switch(change.type) {
                    case "modified":
                        $("#0").replace(displayResults("searchResults", change.doc, change.doc.data().author))
                        break;
                }
                /*
                if (change.type === "added") {
                    console.log("New city: ", change.doc.data());
                }
                if (change.type === "modified") {
                    console.log("Modified city: ", change.doc.data());
                }
                if (change.type === "removed") {
                    console.log("Removed city: ", change.doc.data());
                }
             
            });
        });

 **/

/**
 * This function changes input string to N/A if empty
 */

function checkEmpty(first) {
    let str;
    if (first.replace(/\s/g, "").length == 0) {
        str = "N/A"
    }
    else {
        str = first;
    }
    return str
}

/**
 * This function adds a space after each comma in the category section
 */
function spaces(category_array) {
    const arr = [];
    category_array.forEach((category) => {
        arr.push(" " + category)
    });
    return arr;

}

/**
 * this function takes a string seperated by commas and returns an array
 */

function splitArray(str) {
    const arr = [];
    let array = str.replace(/\s/g, "").split(',');
    array.forEach((word) => {
        arr.push(word);
    })
    return arr
}

class readClubs {
    render(type, elementId, club, school, collection) {
        //data
        let time = DOMPurify.sanitize(checkEmpty(club.data().time), { ALLOWED_TAGS: [], KEEP_CONTENT: true });
        let meets = DOMPurify.sanitize(checkEmpty(club.data().meets), { ALLOWED_TAGS: [], KEEP_CONTENT: true });
        let name = DOMPurify.sanitize(club.data().name, { ALLOWED_TAGS: [], KEEP_CONTENT: true });
        let categories = DOMPurify.sanitize(spaces(club.data().category), { ALLOWED_TAGS: [], KEEP_CONTENT: true });
        let room = DOMPurify.sanitize(checkEmpty(club.data().room), { ALLOWED_TAGS: [], KEEP_CONTENT: true });
        let summary = DOMPurify.sanitize(club.data().description, { ALLOWED_TAGS: [], KEEP_CONTENT: true });

        let detailElement = document.createElement("details");
        // for edit & delete purposes
        let this_id = club.id + collection;
        detailElement.id = this_id;

        let titleElement = document.createElement("summary");
        let summaryTitle = document.createElement("span");
        summaryTitle.className = "summary-title";
        summaryTitle.innerHTML = school + " " + name;
        titleElement.appendChild(summaryTitle);

        let iconParent = document.createElement("div");
        iconParent.className = "summary-chevron-up";
        let icon = document.createElement("i");
        icon.className = "material-icons accented-icons";
        icon.innerHTML = "expand_more";
        iconParent.appendChild(icon);
        titleElement.appendChild(iconParent);

        //categories
        let categoryElement = document.createElement("div");
        let categoryIcon = document.createElement("i");
        categoryIcon.className = "material-icons manage-icons";
        categoryIcon.innerHTML = "category";

        let categoryText = document.createElement("span");
        categoryText.id = this_id + "_category";
        categoryText.className = "text-box";
        categoryText.innerHTML = categories;

        categoryElement.appendChild(categoryIcon);
        categoryElement.appendChild(categoryText);

        // actual summary element
        let summaryDiv = document.createElement("div");
        summaryDiv.className = "summary-content";
        summaryDiv.appendChild(categoryElement);
        let summaryText = document.createElement("span");
        summaryText.className = "text-box";
        summaryText.id = this_id + "_summary";
        summaryText.innerHTML = summary;


        // dropdown icon
        let summaryDivIcon = document.createElement("div");
        summaryDivIcon.className = "summary-chevron-down";

        let summaryIcon = document.createElement("i");
        summaryIcon.className = "material-icons accented-icons";
        summaryIcon.innerHTML = "expand_less";
        summaryDivIcon.appendChild(summaryIcon);

        // time
        let timeElement = document.createElement("div");
        let timeIcon = document.createElement("i");
        timeIcon.className = "material-icons manage-icons";
        timeIcon.innerHTML = "alarm";

        let timeText = document.createElement("span");
        timeText.innerHTML = time;
        timeText.id = this_id + "_time";
        timeText.className = "text-box";

        timeElement.appendChild(timeIcon);
        timeElement.appendChild(timeText);

        //room
        let roomElement = document.createElement("div");
        let roomIcon = document.createElement("i");
        roomIcon.className = "material-icons manage-icons";
        roomIcon.innerHTML = "room";

        let roomText = document.createElement("span");
        roomText.innerHTML = room;
        roomText.className = "text-box";
        roomText.id = this_id + "_room";
        roomElement.appendChild(roomIcon);
        roomElement.appendChild(roomText);

        //meets
        let meetsElement = document.createElement("div");
        let meetsIcon = document.createElement("i");
        meetsIcon.className = "material-icons manage-icons";
        meetsIcon.innerHTML = "today";

        let meetsText = document.createElement("span");
        meetsText.className = "text-box";
        meetsText.id = this_id + "_meets";
        meetsText.innerHTML = meets;

        meetsElement.appendChild(meetsIcon);
        meetsElement.append(meetsText);

        let appendTo = document.getElementById(elementId);
        switch (type) {
            case "info":
                //edit button

                let editDelete = document.createElement("div");
                editDelete.className = "edit_delete";
                let editIcon = document.createElement("i");
                editIcon.className = "material-icons";
                editIcon.id = this_id + "_edit"
                editIcon.innerHTML = "create";

                editDelete.appendChild(editIcon);

                // delete button
                let deleteIcon = document.createElement("i");
                deleteIcon.className = "material-icons";
                deleteIcon.id = this_id + "_delete";
                deleteIcon.innerHTML = "delete";

                editDelete.appendChild(deleteIcon);

                detailElement.appendChild(titleElement);

                summaryDiv.appendChild(timeElement);
                summaryDiv.appendChild(roomElement);
                summaryDiv.appendChild(meetsElement);
                summaryDiv.appendChild(summaryText);
                summaryDiv.appendChild(editDelete);

                detailElement.appendChild(summaryDiv);
                detailElement.appendChild(summaryDivIcon);


                appendTo.appendChild(detailElement);
                break;

            case "submission":
                //status box

                let status = document.createElement("div");
                status.className = "edit_delete";

                // approved button
                let approved = document.createElement("i");
                approved.className = "material-icons";
                approved.id = this_id + "_approve"
                approved.innerHTML = "thumb_up";

                //Fetches from submission db and yeets it into the formal school thing
                approved.onclick = function() {
                    firestore.collection("Counters").doc("0").get()
                        .then(item => {
                            let clubNum = +item.data().clubs + 1;
                            let idx = clubNum + "";
                            firestore.collection("Counters").doc("0").update({
                                "clubs": idx
                            });

                            firestore.collection("Schools").doc(`${clubNum}`).set({
                                "category": categories.split(", "),
                                "description": summary,
                                "keywords": generateKeywords(name),
                                "meets": meets,
                                "name": name,
                                "room": room,
                                "school": school,
                                "time": time
                            }).catch(function(err) {
                                console.error(err);
                            })

                            unapprove(club.id, collection);
                        });
                    
                }

                status.appendChild(approved);

                // unnaproved button
                let unapproved = document.createElement("i");
                unapproved.className = "material-icons";
                unapproved.id = this_id + "_unapprove";
                unapproved.innerHTML = "thumb_down";

                unapproved.onclick = function() {
                    unapprove(club.id, collection);
                }

                status.appendChild(unapproved);

                detailElement.appendChild(titleElement);

                summaryDiv.appendChild(timeElement);
                summaryDiv.appendChild(roomElement);
                summaryDiv.appendChild(meetsElement);
                summaryDiv.appendChild(summaryText);
                summaryDiv.appendChild(status);

                detailElement.appendChild(summaryDiv);
                detailElement.appendChild(summaryDivIcon);


                appendTo.appendChild(detailElement);
                break;
        }
    }

    // change info text to input fields for edit button click
    inputFieldsWithInfo(clubId, collection) {
        // time, room, meets, summary
        let id = "#" + clubId.toString() + collection;
        let toTransform = ["time", "room", "meets", "summary", "category"];
        let infoShuttle = {};
        // info in db
        firestore.collection(collection).doc(clubId).get().then(function (club) {
            infoShuttle.time = club.data().time;
            infoShuttle.room = club.data().room;
            infoShuttle.meets = club.data().meets;
            infoShuttle.summary = club.data().description;
            infoShuttle.category = club.data().category;
            for (var i in toTransform) {
                let element = toTransform[i];
                let input;
                if (element !== "summary") {
                    input = document.createElement("input");
                }
                else {
                    input = document.createElement("textarea");
                }
                input.className = "club-field";
                input.id = clubId + collection + "_" + element + "-field";
                input.value = infoShuttle[element];
                $("span" + id + "_" + element + ".text-box").replaceWith(input);
            }

        });

        // change edit button to cancel button
        let cancelButton = document.createElement("i");
        cancelButton.id = clubId + collection + "_cancel";
        cancelButton.className = "material-icons";
        cancelButton.innerHTML = "cancel";
        $(id + "_edit").replaceWith(cancelButton);

        $(id + "_cancel").on('click', function () {
            cancelClubClicked(clubId, collection);
            console.log("cancel club clickeds")
        });

        // change delete button to done button
        let doneButton = document.createElement("i");
        doneButton.id = clubId + collection + "_done";
        doneButton.className = "material-icons";
        doneButton.innerHTML = "check_circle";
        $(id + "_delete").replaceWith(doneButton);

        $(id + "_done").on('click', function () {
            doneClubClicked(clubId, collection);
            console.log("done clicked");
        });
    }

    fillBackWithInfo(clubId, collection) {
        // time, room, meets, summary
        let toTransform = ["time-field", "room-field", "meets-field", "summary-field", "category-field"];
        let infoShuttle = {};
        let this_id = clubId + collection;
        // info in db 
        firestore.collection(collection).doc(clubId).get().then(function (club) {
            infoShuttle.time = club.data().time;
            infoShuttle.room = club.data().room;
            infoShuttle.meets = club.data().meets;
            infoShuttle.summary = club.data().description;
            infoShuttle.category = club.data().category;

            for (var i in toTransform) {
                let element = toTransform[i];
                let id = "#" + this_id + "_" + element;
                let textSpan = document.createElement("span");
                textSpan.id = this_id + "_" + element.split("-")[0];
                textSpan.className = "text-box";

                if (element !== "category-field") {
                    textSpan.innerHTML = checkEmpty(infoShuttle[element.split('-')[0]]);
                }

                else {
                    textSpan.innerHTML = spaces(infoShuttle[element.split('-')[0]])
                }
                $(id + ".club-field").replaceWith(textSpan);
                console.log(textSpan.id);
            }

        });
    }

}



function editClubClicked(clubId, collection) {
    showClubs.inputFieldsWithInfo(clubId, collection);
}

function doneClubClicked(clubId, collection) {
    let toSet = ["time-field", "room-field", "meets-field", "summary-field", "category-field"];

    let id = "#" + clubId + collection + "_";
    let infoShuttle = {};
    for (var i in toSet) {
        let field = toSet[i];
        infoShuttle[field.split('-')[0]] = $(id + field).val();
        console.log(field);
    }
    firestore.collection(collection).doc(clubId).set({
        time: DOMPurify.sanitize(infoShuttle.time, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        room: DOMPurify.sanitize(infoShuttle.room, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        meets: DOMPurify.sanitize(infoShuttle.meets, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        description: DOMPurify.sanitize(infoShuttle.summary, { ALLOWED_TAGS: [], KEEP_CONTENT: true }),
        category: splitArray(infoShuttle.category)
    }, { merge: true });

    cancelClubClicked(clubId, collection);
}

function deleteClubClicked(clubId, collection) {
    firestore.collection(collection).doc(clubId).delete().then(function () {
        console.log("Document successfully deleted!");
    }).catch(function (error) {
        console.error("Error removing document: ", error);
    });
}

function cancelClubClicked(clubId, collection) {

    showClubs.fillBackWithInfo(clubId, collection);
    let id = "#" + clubId.toString() + collection;
    let this_id = clubId.toString() + collection;

    // change cancel button to edit button
    let editButton = document.createElement("i");
    editButton.id = this_id + "_edit";
    editButton.className = "material-icons";
    editButton.innerHTML = "create";
    $(id + "_cancel").replaceWith(editButton);

    // change done button to delete button
    let deleteButton = document.createElement("i");
    deleteButton.id = this_id + "_delete";
    deleteButton.className = "material-icons";
    deleteButton.innerHTML = "delete";
    $(id + "_done").replaceWith(deleteButton);

    addEditEvents(clubId, collection);
}

let showClubs = new readClubs;
class makeItLazy {

    lazy(type, elementId, collection) {
        let indexTransport = {};
        let indexesRendered = [];
        let showClubs = new readClubs;

        function lazyload(lastIndex, limit) {
            firestore.collection(collection).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    if (!indexesRendered.includes(doc.id) && doc.data().display !== "none") {
                        showClubs.render(type, elementId, doc, doc.data().school, collection);
                        switch (type) {
                            case "info":
                                addEditEvents(doc.id, collection);
                                break;
                            case "submission":
                                addStatusEvents(doc.id, collection);
                                break;
                        }
                    }
                    indexesRendered.push(doc.id);
                });
            });
            lastIndex = parseFloat(lastIndex) + 1;
            lastIndex = lastIndex.toString();
            indexTransport.lastIndex = lastIndex;
            indexTransport.lastIndex = parseFloat(indexTransport.lastIndex) + 1;
        }

        window.addEventListener("scroll", lazyloadHelper);

        function lazyloadHelper() {
            let lastDoc = indexTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {
                lastDoc = parseFloat(lastDoc) + 1;
                lazyload(lastDoc.toString(), 4);
            }
        }

        lazyload("0", 15);
    }
}

