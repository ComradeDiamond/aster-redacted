function kickoff() {
    var container = document.createElement('ul');
    container.id = "container";
    container.className = "cards";
    $("#browse-container").append(container);
    lazyload("container", "0", 6);
}
/**
 *
 * @param {Array} data An array of clubs to be filtered through
 * @returns {Array} An array of clubs after data was processed
 *
 * This function collects the filters selected by the user and returns an array after filtering the inputted array using user selected filters
 */


function searchData(random) {
    let search_value = $("#search").val();
    let $categories = [];
    let $schools = [];
    let $days = [];
    let $categoriesSelected = $('input[name="category[]"]:checked');
    let $schoolsSelected = $('input[name="school[]"]:checked');
    let $daysSelected = $('input[name="week[]"]:checked');
    if (document.getElementById("lst").checked) {
        var onListOnly = true;
    } else {
        var onListOnly = false;
    }

    if ($schoolsSelected.length > 0) {
        $schoolsSelected.each(function () {
            $schools.push($(this).val());
        });
    }
    else {
        $schools = [];
    }


    if ($categoriesSelected.length > 0) {
        $categoriesSelected.each(function () {
            $categories.push($(this).val());
        });

    }
    else {
        $categories = [];
    }

    if ($daysSelected.length > 0) {
        $daysSelected.each(function () {
            $days.push($(this).val());
        });

    }
    else {
        $days = [];
    }

    indexTransport = {};
    indexesRendered = [];

    if (random) { //picking random
        if (onListOnly) {
            // check which cases to execute
            var randFilter = new RandomFilter();
            //default
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                let indexesRendered = [];
                let count = [];
                let processed = 0;

                let getData = new Promise(function (resolve, reject) {
                    let subarray = interestedClubs;
                    subarray.forEach(async function (club) {
                        let splitted = club.split("/");

                        let school = splitted[0];
                        let name = splitted[1];

                        await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && ($days.length == 0 || meetsOnDay(doc, days)) && (!onListOnly || interestedClubs.indexOf(doc.data().school + "/" + doc.data().name) > -1)) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    count.push(snapDoc.data().link);
                                }
                            });
                        });
                        if (processed == subarray.length) resolve();
                    });
                });

                getData.then(function () {
                    if (count.length == 0) return mdcWarn("Try another query");
                    location.href = count[Math.floor(Math.random() * count.length)];
                });
            }

            // by name only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                randFilter.byNameAndInterest(search_value, $days);
            }

            //category only
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                randFilter.byCategoryAndInterest($categories, $days);
            }

            // school only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                randFilter.bySchoolAndInterest($schools, $days);
            }

            //category and name

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                randFilter.byNameAndCategoryAndInterest(search_value, $categories, $days);
            }

            // category and school

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                randFilter.byCategoryAndSchoolAndInterest($schools, $categories, $days);
            }

            // by school and name
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                randFilter.bySchoolAndNameAndInterest($schools, search_value, $days);
            }

            // by school, name, and category
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                randFilter.byNameAndCategoryAndSchoolAndInterest($schools, $categories, search_value, $days)
            }

        } else {
            // check which cases to execute
            var randFilter = new RandomFilter();
            //default
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                var count = [];
                db.collection("Schools").orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        if (!indexesRendered.includes(doc.id) && ($days.length == 0 || meetsOnDay(doc, $days))) {
                            count.push(doc.data().link);
                        }
                        indexesRendered.push(doc.id);
                    });
                }).then(function () {
                    if (count.length == 0) return mdcWarn("Try another query");
                    location.href = count[Math.floor(Math.random() * count.length)];
                });
            }

            // by name only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                randFilter.byName(search_value, $days);
            }

            //category only
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                randFilter.byCategory($categories, $days);
            }

            // school only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                randFilter.bySchool($schools, $days);
            }

            //category and name

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                randFilter.byNameAndCategory(search_value, $categories, $days);
            }

            // category and school

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                randFilter.bySchoolAndCategory($schools, $categories, $days);
            }

            // by school and name
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                randFilter.bySchoolAndName($schools, search_value, $days);
            }

            // by school, name, and category
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                randFilter.bySchoolNameCategory($schools, $categories, search_value, $days)
            }

        }
    } else { //not random

        $("#container").remove();
        $("#searchResults").remove();
        let search = new Filter;

        var results = document.createElement('ul');
        results.id = "searchResults";
        results.className = "cards";
        $("#browse-container").append(results);

        // check which cases to execute

        if (onListOnly) {
            //default
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                var toRemove = document.getElementsByClassName("applyButton").handler; //obtain the reference to old
                if (toRemove != undefined) { //prevents error from removing undefined
                    window.removeEventListener("scroll", toRemove); //remove old
                }

                if ($days.length != 0 || onListOnly) {
                    let dataTransport = {};
                    let indexesRendered = [];
                    let lastDoc;
                    let availableOnFirestore = 0;
                    let tempHasRendered = 0;
                    let processed = 0;

                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }

                    function load(lastIndex, limit) {
                        availableOnFirestore = 0;
                        tempHasRendered = 0;
                        processed = 0;

                        let getData = new Promise(function (resolve, reject) {
                            let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                            subarray.forEach(async function (club) {
                                let splitted = club.split("/");

                                let school = splitted[0];
                                let name = splitted[1];

                                processed++;

                                await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                                    querySnapshot.forEach(function (doc) {
                                        if (!indexesRendered.includes(doc.id) && ($days.length == 0 || meetsOnDay(doc, $days))) {
                                            indexesRendered.push(doc.id);
                                            console.log(doc.id, " => ", doc.data().keywords);
                                            cardDisplay("searchResults", doc, doc.data().school);
                                            tempHasRendered++;
                                        }
                                        availableOnFirestore++;
                                    });
                                });


                                if (processed == subarray.length) resolve();
                            });
                        });

                        getData.then(function () {
                            lastIndex = parseFloat(lastIndex) + limit;
                            lastIndex = lastIndex.toString();
                            dataTransport.lastIndex = lastIndex;

                            if (availableOnFirestore > 0 && tempHasRendered == 0) {
                                lastDoc = dataTransport.lastIndex;
                                lastDoc = parseFloat(lastDoc) + 1;
                                return search(lastDoc.toString(), 6);
                            } else if (indexesRendered.length == 0) {
                                mdcWarn("Try a different query");
                                var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                                if (toRemove != undefined) { //prevents error from removing undefined
                                    window.removeEventListener("scroll", toRemove); //remove old
                                }
                                return;
                            }

                            window.addEventListener("scroll", helper); //add new
                            document.getElementsByClassName("applyButton")[0].handler = helper;

                        });
                    }

                    function helper() {

                        lastDoc = dataTransport.lastIndex;
                        let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
                        if (scrollAtBottom) {

                            lastDoc = parseFloat(lastDoc) + 1;
                            load(lastDoc.toString(), 4);

                        }
                    }
                    load("0", 6);


                } else {
                    var container = document.createElement('ul');
                    container.id = "container";
                    container.className = "cards";
                    $("#browse-container").append(container);

                    lazyload("container", "0", 6);
                }
            }

            // by name only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                search.byCategoryAndInterest(search_value, $days);
            }

            //category only
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                search.byCategoryAndInterest($categories, $days);
            }

            // school only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                search.bySchoolAndInterest($schools, $days);
            }

            //category and name

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                search.byNameAndCategoryAndInterest(search_value, $categories, $days);
            }

            // category and school

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                search.byCategoryAndSchoolAndInterest($schools, $categories, $days);
            }

            // by school and name
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                search.bySchoolAndNameAndInterest($schools, search_value, $days);
            }

            // by school, name, and category
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                search.byNameAndCategoryAndSchoolAndInterest($schools, $categories, search_value, $days)
            }

        } else {
            //default
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                var toRemove = document.getElementsByClassName("applyButton").handler; //obtain the reference to old
                if (toRemove != undefined) { //prevents error from removing undefined
                    window.removeEventListener("scroll", toRemove); //remove old
                }

                if ($days.length != 0 || onListOnly) {
                    let dataTransport = {};
                    let indexesRendered = [];
                    let lastDoc;
                    let availableOnFirestore = 0;
                    let tempHasRendered = 0;

                    function load(lastIndex, limit) {
                        availableOnFirestore = 0;
                        tempHasRendered = 0;

                        db.collection("Schools").orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && ($days.length == 0 || meetsOnDay(doc, $days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        }).then(function () {
                            lastIndex = parseFloat(lastIndex) + limit;
                            lastIndex = lastIndex.toString();
                            dataTransport.lastIndex = lastIndex;

                            if (availableOnFirestore > 0 && tempHasRendered == 0) {
                                lastDoc = dataTransport.lastIndex;
                                lastDoc = parseFloat(lastDoc) + 1;
                                return load(lastDoc.toString(), 6);
                            } else if (indexesRendered.length == 0) {
                                mdcWarn("Try a different query");
                                var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                                if (toRemove != undefined) { //prevents error from removing undefined
                                    window.removeEventListener("scroll", toRemove); //remove old
                                }
                                return;
                            }

                            window.addEventListener("scroll", helper); //add new
                            document.getElementsByClassName("applyButton")[0].handler = helper;
                        });

                    }

                    function helper() {
                        lastDoc = dataTransport.lastIndex;
                        let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
                        if (scrollAtBottom) {

                            lastDoc = parseFloat(lastDoc) + 1;
                            load(lastDoc.toString(), 4);

                        }
                    }
                    load("0", 6);

                } else {
                    var container = document.createElement('ul');
                    container.id = "container";
                    container.className = "cards";
                    $("#browse-container").append(container);

                    lazyload("container", "0", 6);
                }
            }

            // by name only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                search.byName(search_value, $days);
            }

            //category only
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length == 0) {
                search.byCategory($categories, $days);
            }

            // school only
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                search.bySchool($schools, $days);
            }

            //category and name

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length == 0) {
                search.byNameAndCategory(search_value, $categories, $days);
            }

            // category and school

            if ($categories.length > 0 && search_value.replace(/\s/g, "").length == 0 && $schools.length > 0) {
                search.bySchoolAndCategory($schools, $categories, $days);
            }

            // by school and name
            if ($categories.length == 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                search.bySchoolAndName($schools, search_value, $days);
            }

            // by school, name, and category
            if ($categories.length > 0 && search_value.replace(/\s/g, "").length > 0 && $schools.length > 0) {
                search.bySchoolNameCategory($schools, $categories, search_value, $days)
            }

        }
    }
}

$(document).ready(function () {
    document.getElementById("search-btn").addEventListener("click", function () {
        searchData(false);
    });
    document.getElementById("search").addEventListener("keydown", function (event) {
        if (event.key == "Enter") {
            searchData(false);
        }
    });
    document.getElementById("random-btn").addEventListener("click", function () {
        searchData(true);
    });
});
