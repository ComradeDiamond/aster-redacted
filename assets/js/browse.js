$(".filter_link").on('click', function () {
  $(".filter_menu").toggleClass('toggleDisplay');

  var filterLinkElement = document.getElementsByClassName("filter_link")[0];

  if ($(".filter_menu").hasClass('toggleDisplay')) {

    var close_menu = '<i class="material-icons-outlined align-icons">cancel</i> Hide Filters';
    filterLinkElement.innerHTML = close_menu;
    filterLinkElement.style.background = "var(--primary-color)";
    filterLinkElement.style.color = "var(--color-white)";
  }

  else {
    var open_menu = '<i class="material-icons-outlined align-icons">tune</i> Advanced Filters';
    document.getElementsByClassName("filter_link")[0].innerHTML = open_menu;
    filterLinkElement.innerHTML = open_menu;
    filterLinkElement.style.background = "";
    filterLinkElement.style.color = "";
  }
});

