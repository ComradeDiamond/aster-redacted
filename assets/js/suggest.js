

class userCustomized {
    /**
 * suggested clubs algorithm
 *  @param {string} elementId the id of the  parent element container
 * @param {string} user_id  the firebase uid of the signed in user to reccomend clubs to
 */
    suggest(elementId, user_id) {
        let userRef = firebase.database().ref('users/').child(user_id);
        let rendered = [];
        userRef.once('value', function(snapshot){
            if (snapshot.hasChild("suggestCategories")) {
               let categoriesToRecomend = JSON.parse(snapshot.val().suggestCategories);

                db.collection("Schools").where('category', 'array-contains-any', categoriesToRecomend).limit(4).get().then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        if(!rendered.includes(doc.id)) {
                            cardDisplay(elementId, doc, doc.data().school);
                            rendered.push(doc.id);
                        }
                    });
                }); 
            }
        });
          
                /*TODO do not include suggestions that are already in user's list
                   if (snapshot.hasChild("recent")) {
                    var tempCategories = JSON.parse(snapshot.val().recent);
                } else {
                    var tempCategories = [];
                }
                let tempInterest = JSON.parse(snapshot.val().interestedClubs);
                let tempConcat = tempInterest.concat(tempCategories);

                for(var interest = 0; interest < 8; interest++){
                    let tempSchool = tempConcat[interest].split('/')[0];
                    let tempName = tempConcat[interest].split('/')[1];
                    await db.collection("Schools").where('school', '==', tempSchool).where('name', '==', tempName).get().then(function(doc){
                        doc.forEach(function (snap) {
                            for(var i in snap.data().category) {
                                if(!categoriesToRecomend.includes(snap.data().category[i])) {
                                    categoriesToRecomend.push(snap.data().category[i]);
                                }
                              }
                        });
                    })
                }

                    if(tempInterest.indexOf(this.school + "/" + this.name) < 0){

                    }
                    */

      }
     /**
         * show last five browsed clubs
         *  @param {string} elementId the id of the  parent element container
         * @param {string} user_id  the firebase uid of the signed in user to show recent clubs to
    */
    renderRecent(elementId, user_id) {
       let recentList = [];
       let userRef = firebase.database().ref('users/').child(user_id);
       userRef.once('value', function(snapshot){ 
            if (snapshot.hasChild("recent")) {
                recentList = JSON.parse(snapshot.val().recent);
            } else {
                console.log("nada. zip. finito");
            }
        });
        
        recentList.reverse();
        for (
            var i in recentList
          ) {
            let tempList = recentList[i];
            if (tempList == undefined) return;
            let school = tempList.split("/")[0];
            let clubname = tempList.split("/")[1];
            db.collection("Schools")
              .where("name", "==", clubname)
              .where("school", "==", school)
              .get()
              .then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    cardDisplay(elementId, doc, doc.data().school);
                  
                });
              });
          }
    }
 }
 function kickoff(){
    let customSuggest = new userCustomized;
    customSuggest.suggest("suggestedClubs", firebase.auth().currentUser.uid);
    customSuggest.renderRecent("recentClubs", firebase.auth().currentUser.uid);
 }
 /**
  *        // takes too long to load model, see how to spead it up
        // let tempConcat = recentCategories.concat(interestCategories, synonyms(recentCategories), synonyms(interestCategories));
   
        // remove possible duplicates in the list
        let uniqueCategories = tempConcat;
        // get similar possibilities
        console.log(recentCategories);
        
        
        // get clubs with the same categories, limit to 5
        firestore.collection("Schools").where('category', 'array-contains-any', uniqueCategories).limit(4).get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                cardDisplay(elementId, doc, doc.data().school);
            });
        });
  */