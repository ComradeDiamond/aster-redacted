$(window).on('load', function () {
        var modal = document.getElementById("thankModal");
    
        $(".close").on('click', function () {
            modal.style.display = "none";
        });
    
        /*
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }*/
        firebase.auth().onAuthStateChanged(function (user) {
            if (!user) location.href = "/login";
        });
    });
     function initUpload(e, devParse) {
        if (devParse)
        {
            if (imageArray.length == maxImages)
            {
                mdcWarn(`You can't insert more than ${maxImages} images`);
                return;
            }

            imageArray.push(e.files[0]);
        }

        uploadDiv.innerHTML = "<br />";

        for (let i in imageArray) {
            let divHolder = document.createElement("div");
            divHolder.className = "divHolderCSS";

            let spanX = document.createElement("span");
            spanX.innerHTML = "cancel";
            spanX.classList.add("deleteX");
            spanX.classList.add("material-icons");
            spanX.classList.add("align-icons");
            spanX.addEventListener("click", () => {
                imageArray.splice(i, 1);
                initUpload(e, false);
            });

            let spanContent = document.createElement("span");
            spanContent.innerText = `   ${imageArray[i].name}`;

            divHolder.appendChild(spanX);
            divHolder.appendChild(spanContent);

            uploadDiv.appendChild(divHolder);
        }
    }
    function submit() {
        var categories = [];
        let $categoriesSelected = $('input[name="category[]"]:checked');
    
        $categoriesSelected.each(function () {
            categories.push($(this).val());
        });
    
        let modal = document.getElementById("thankModal");
        let form = document.getElementById("contact_form");
        let modalcontent = document.getElementById("modal-text");
        let modalbtn = document.getElementById("modal-btn");
    
        if (!form.checkValidity()) {
            console.log()
            modal.style.display = "flex";
            return; 
        }
        let name = form.clubname.value;
        let school = form.schoolname.value;

        //get user info
        let imageArrayIdx = [];
        var user = firebase.auth().currentUser;
        var userRef = firebase.database().ref('/users/').child(user.uid);
        userRef.once("value", (snapshot) => {
            var info = snapshot.val();

            contactName = `${info.firstname} ${info.lastname}`;
        }).then(function() {
            //Store firebase photos
            for (var c in imageArray)
            {
                var sRef = firecloud.ref().child("clubImages/" + `${school}-${name}-${firebase.auth().currentUser.uid}-${imageArray[c].name}`);
                try {
                    sRef.put(imageArray[c])
                    imageArrayIdx.push(sRef.location.path);
                    console.log("success!");
                }
                catch(err)
                {
                    console.log(err);
                }
            }
        }).then(function() {
            firestore.collection("Submissions").doc(school + " - " + name).set({
                "contact_name": DOMPurify.sanitize(contactName, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "name": DOMPurify.sanitize(name, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "contact_email": firebase.auth().currentUser.email,
                "uid": firebase.auth().currentUser.uid,
                "school": DOMPurify.sanitize(school, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "category": categories,
                "president": DOMPurify.sanitize(form.president.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "president_email": form.prezemail.value,
                "advisor": DOMPurify.sanitize(form.adv.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "advisor_email": form.advemail.value,
                "room": DOMPurify.sanitize(form.rm.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "description": DOMPurify.sanitize(form.message.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "time": DOMPurify.sanitize(form.time.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "meets": DOMPurify.sanitize(form.meet.value, {ALLOWED_TAGS: [], KEEP_CONTENT: true}),
                "submission_time": Date.now(),
                "image_locations" : imageArrayIdx
            }).then(function () {
                modalcontent.innerHTML = "Club request submitted. Thanks for contributing!";
                modalbtn.onclick = () => location.reload();
        
                modal.style.display = "flex";
            }).catch(function (error) {
        
                console.log(error);
                modalcontent.innerHTML = "This club has already been requested or already exists";
        
                modal.style.display = "flex";
            });
        });
    }