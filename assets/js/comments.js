shuttle = {};

const defaultImg = "/assets/images/default.PNG";
var commentsRef = firebase.database().ref('comments/');
var postRef = commentsRef.child(slugify(window.location.pathname));

function setFirst() {

	let executed = false;

	if (!executed) {
		executed = true;
		postRef.on("value", function (snapshot) {
			try {
				var post = snapshot.val();
				var test = post.rating;
				test;
			} catch (TypeError) {
				postRef.child("page_info").set({
					rating: 0,
					permalink: window.location.pathname,
					name: document.title
				});
			}
		});
	}

}
function slugify(text) {
	return text.toString().toLowerCase().trim()
		.replace(/&/g, '-and-')
		.replace(/[\s\W-]+/g, '-')
		.replace(/[^a-zA-Z0-9-_]+/g, '');
}

/** Function to display rating on info page */
function pageDisplay(rating) {
	var rating_para = document.createElement('p');
	var comment_rating = document.createElement('i');
	comment_rating.className = "fas fa-star";

	rating_para.append(rating.toString());
	rating_para.append(comment_rating);
	$("#displayRating").append(rating_para);
}

function escapeHtml(str) {
	return "<span>" + str + "</span>";
}

function deleteButtonClick() {
	var user = shuttle.user;
	var userComment = postRef.child(user.uid);
	userComment.remove()
		.then(function () {
			$('#comment').trigger("reset");
			$("#mdc-outline").removeClass('mdc-text-field--invalid');
			let removeId = user.uid.toString() + "_parent";
			document.getElementById(removeId).remove();
			$("#comment_list").show();
			console.log("Remove succeeded.");
			mdcWarn("Comment was removed");
		})
		.catch(function (error) {
			console.log("Remove failed: " + error.message);
		});
	//recalc the page_rating

	let scopedShuttle = {};
	scopedShuttle.ifOnly = false;
	postRef.on('value', function (snapshot) {
		if (snapshot.numChildren() == 2) {
			scopedShuttle.ifOnly = true;
		}
	});

	let updatedRating = loadStars();
	if (!scopedShuttle.ifOnly) {
		updatedRating = 0;
	}
	postRef.child("page_info").update({
		rating: updatedRating
	});

	// show recalc'ed page rating
	$("#displayRating").empty();
	pageDisplay(loadStars());
}

function doneButtonClick() {
	let name;

	let renderEdit = new Comments;
	var user = firebase.auth().currentUser;
	var userRef = firebase.database().ref('users/' + user.uid);

	userRef.once("value", function (snapshot) {
		name = DOMPurify.sanitize(snapshot.val().username, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });

	});


	var star_val = document.getElementById("edit-form").edit_stars.value;
	if (star_val == "") {
		return;
	}
	console.log($("#edited").val());

	shuttle.newMsg.innerHTML = DOMPurify.sanitize(document.getElementById("edited").value, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });
	var sanitized = DOMPurify.sanitize(document.getElementById("edited").value, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });

	$("#done").remove();
	$("#cancel").remove();

	$("#edit").remove();
	$("#delete").remove();
	//empty users prev comment
	$("#" + user.uid.toString() + "_parent").remove();

	//change to edited version
	renderEdit.addTo("comment_div", user.uid, name, shuttle.newMsg, star_val);
	renderEdit.reportEvents(user.uid, shuttle.newMsg);
	// add edit grid
	renderEdit.editGrid(user.uid.toString());
	//add click events
	renderEdit.clickEvents();

	if (sanitized.replace(/\s/g, "").length > 0) {
		postRef.once("value", function () {
			postRef.child(user.uid).update({
				message: sanitized,
				name: name,
				rating: parseInt(star_val)
			});

			console.log("added");
		});
		postRef.child("page_info").update({
			rating: loadStars()
		});
	}
	else {
		deleteButtonClick();
	}
	$("#edit-form").remove();
	$("new-rating").remove();

	for (var br of document.getElementsByClassName("tmpBr")) {
		br.remove();
	}
	for (var br of document.getElementsByClassName("tmpBr")) {
		br.remove();
	}
	for (var br of document.getElementsByClassName("tmpBr2")) {
		br.style.display = "initial";
	}

	// show recalc'ed page rating
	$("#displayRating").empty();
	pageDisplay(loadStars());
}

function cancelButtonClick() {
	$("#new-rating").remove();
	$("#edit-form").remove();


	$("#done").remove();
	$("#cancel").remove();

	$("#edit").remove();
	$("#delete").remove();

	var user = firebase.auth().currentUser;
	$("#" + user.uid.toString() + "_parent").remove();

	const renderCancel = new Comments;
	postRef.child(user.uid.toString()).once("value", function (snapshot) {
		console.log(snapshot.val().username);

		//change to prev version
		renderCancel.addTo("comment_div", user.uid, snapshot.val().name, snapshot.val().message, snapshot.val().rating);
		// add edit grid
		renderCancel.editGrid(user.uid.toString());
		//add click events
		renderCancel.clickEvents();
		//add report
		renderCancel.reportEvents(user.uid, snapshot.val().message);


	});
	for (var br of document.getElementsByClassName("tmpBr")) {
		br.remove();
	}
	for (var br of document.getElementsByClassName("tmpBr")) {
		br.remove();
	}
	for (var br of document.getElementsByClassName("tmpBr2")) {
		br.style.display = "initial";
	}
}

function editButtonClick() {
	// star field
	var user = firebase.auth().currentUser;
	let uid = user.uid.toString();
	var current_message = $("#" + uid + "> p").html();
	let input_field = document.createElement("form");
	input_field.id = "edit-form";

	shuttle.currentMsg = current_message;

	let stars_label = document.createElement("label");
	stars_label.for = "new-rating";
	input_field.append(stars_label);

	let div_rating = document.createElement("div");
	div_rating.id = "new-rating";
	div_rating.className = "star-rating";

	// loop to generate the inputs rating
	let starIds = ["e5", "e4", "e3", "e2", "e1"];
	for (starId in starIds) {
		let inputId = starIds[starId];
		let newInput = document.createElement("input");
		newInput.type = "radio";
		newInput.id = inputId;
		newInput.name = "edit_stars";
		newInput.value = inputId.split('')[1];

		let newLabel = document.createElement("label");
		newLabel.htmlFor = inputId;

		let newIcon = document.createElement("i");
		newIcon.className = "fas fa-star";

		newLabel.appendChild(newIcon);

		div_rating.appendChild(newInput);
		div_rating.appendChild(newLabel);
	}

	input_field.append(div_rating);

	var br1 = document.createElement("br");
	br1.className = "tmpBr";
	var br2 = document.createElement("br");
	br2.className = "tmpBr";
	input_field.appendChild(br1);
	input_field.appendChild(br2);

	// add edit textfield
	let textfield = document.createElement("div");
	textfield.className = "mdc-text-field mdc-text-field--outlined mdc-text-field--textarea entry mdc-text-field--label-floating mdc-text-field--with-internal-counter";
	textfield.innerHTML = `
	<textarea class="mdc-text-field__input" id="edited" cols="30" rows="5" maxlength="280" required></textarea>
	<span class="mdc-text-field-character-counter">0 / 280</span>
	<div class="mdc-notched-outline mdc-notched-outline--upgraded mdc-notched-outline--notched">
		<div class="mdc-notched-outline__leading"></div>
		<div class="mdc-notched-outline__notch" style="width: 100px;">
		<label for="edited" class="mdc-floating-label mdc-floating-label--float-above">Edit Your Review</label>
		</div>
		<div class="mdc-notched-outline__trailing"></div>
	</div>`;

	input_field.append(textfield);

	$("#" + uid + "> span").empty();
	$("#" + uid + "> p").replaceWith(input_field);

	mdc.textField.MDCTextField.attachTo(textfield);

	// change message to last message
	$('#edited').val(current_message);
	document.getElementById(uid).getElementsByTagName("h4").innerHTML = "Edit your review";

	//previous stars
	postRef.child(uid.toString()).once("value", function (snapshot) {
		document.getElementById("edit-form").edit_stars.value = snapshot.val().rating;
	});

	// show edit and delete buttons
	$("#edit").hide();
	$("#delete").hide();

	// hide done and cancel button
	$("#done").show();
	$("#cancel").show();

	var br1 = document.createElement("br");
	br1.className = "tmpBr";
	var br2 = document.createElement("br");
	br2.className = "tmpBr";
	document.getElementsByClassName("edit_grid")[0].appendChild(br1);
	document.getElementsByClassName("edit_grid")[0].appendChild(br2);

	document.getElementById("done").removeEventListener("click", doneButtonClick);
	document.getElementById("done").addEventListener("click", doneButtonClick);

	document.getElementById("cancel").removeEventListener("click", cancelButtonClick);
	document.getElementById("cancel").addEventListener("click", cancelButtonClick);

	for (var br of document.getElementsByClassName("tmpBr2")) {
		br.style.display = "none";
	}
}

function reportReview(comment_id, message) {
	console.log("Reported: " + comment_id);
	var user = firebase.auth().currentUser;
	if (user) {
		var reportRef = firebase.database().ref('reports');
		reportRef.child(slugify(window.location.pathname) + "/" + user.uid).update({
			reportedUser: comment_id,
			message: message
		}).then(function () { mdcWarn("Thank you for the report.  <br />We will follow up as necessary") });
	}
}
function loadStars() {
	let ratings_array = [];
	var count_votes = 0;

	postRef.on("child_added", function (snapshot) {
		var child_name = snapshot.key;
		if (child_name != "page_info") {
			count_votes += 1;
			ratings_array.push(parseFloat(snapshot.val().rating));
		}

	});

	var star_sum = ratings_array.reduce(function (a, b) {
		return a + b;
	}, 0) * 1 / 5;
	rating = (star_sum / count_votes);

	rating = rating * 5;
	rating = Math.round((rating + .001) * 100) / 100;
	return rating;
}

class Comments {
	addTo(elementId, user_id, name, message, rating) {

		var id = "#" + elementId.toString();

		let container_div = document.createElement("div");
		container_div.className = "parent_comment";
		container_div.id = user_id + "_parent";

		var comment_div = document.createElement('div');
		comment_div.id = user_id;
		comment_div.className = 'comment';

		name = DOMPurify.sanitize(name, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });
		message = DOMPurify.sanitize(message, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });


		var pfp_image = document.createElement("img");
		pfp_image.className = "profile_comment";

		let getPfpPromise = new Promise((resolve, reject) => {
			try {
				var tempSrc = firecloud.ref().child("pfp/" + user_id).getDownloadURL();
				resolve(tempSrc);
			}
			catch (err) {
				reject(err);
			}
		})

		getPfpPromise
			.then((tempSrc) => {
				pfp_image.src = tempSrc;
			})
			.catch(() => {
				pfp_image.src = defaultImg;
			});

		container_div.append(pfp_image);

		var comment_name = document.createElement('h4');
		comment_name.innerHTML += name;

		/* report button */
		var reportDiv = document.createElement("div");
		reportDiv.className = "report";
		reportDiv.id = user_id + "_report";

		var reportFlag = document.createElement("i");
		reportFlag.className = "material-icons accented-icons";
		reportFlag.innerHTML = "outlined_flag";

		reportDiv.appendChild(reportFlag);

		var reportSpan = document.createElement("span");
		reportSpan.innerHTML = "Report Review";

		reportDiv.appendChild(reportSpan);
		comment_name.appendChild(reportDiv);
		comment_div.append(comment_name);


		var rating_para = document.createElement('span');

		var comment_rating = document.createElement('i');
		comment_rating.className = "fas fa-star";

		rating_para.append(rating.toString());
		rating_para.append(comment_rating);
		comment_div.append(rating_para);

		var comment_message = document.createElement('p');
		comment_message.innerHTML += message;
		comment_div.append(comment_message);

		container_div.append(comment_div);
		if (firebase.auth().currentUser) {
			if (firebase.auth().currentUser.uid == user_id) {
				$(id).prepend(container_div);

			} else {
				$(id).append(container_div);
			}
		} else {
			$(id).append(container_div);
		}
	}

	editGrid(id) {
		var comment_id = id;
		firebase.auth().onAuthStateChanged(function (user) {
			if (user && user.uid == comment_id) {
				let id = "#" + comment_id + "";
				let edit_grid = document.createElement('div');
				edit_grid.className = "edit_grid";

				let prepend = document.createElement('div');
				prepend.className = "prepend";
				$(id).append(prepend);

				let edit_link = document.createElement('a');
				edit_link.className = "edit-btn";
				edit_link.innerHTML = "Edit &#160;";
				edit_link.id = "edit";
				$(edit_grid).append(edit_link);

				let delete_link = document.createElement('a');
				delete_link.className = "edit-btn";
				delete_link.innerHTML = "Delete";
				delete_link.id = "delete";
				$(edit_grid).append(delete_link);

				$(id).append(edit_grid);

				var br1 = document.createElement("br");
				br1.className = "tmpBr2";
				var br2 = document.createElement("br");
				br2.className = "tmpBr2";
				document.getElementsByClassName("edit_grid")[0].appendChild(br1);
				document.getElementsByClassName("edit_grid")[0].appendChild(br2);


				console.log("Can edit");
			} else {
				console.log("Can't Edit")
			}
		});

	}

	clickEvents() {

		$(document).ready(function () {
			firebase.auth().onAuthStateChanged(function (user) {
				if (user) {
					shuttle.user = user;

					/* editing events */

					document.getElementById("delete").removeEventListener("click", deleteButtonClick);
					document.getElementById("delete").addEventListener("click", deleteButtonClick);

					document.getElementsByClassName("edit_grid")[0].appendChild(document.createElement("br"));

					let cancel_link = document.createElement('a');
					cancel_link.className = "edit-btn";
					cancel_link.innerHTML = "&#160; Cancel";
					cancel_link.id = "cancel";
					$(".edit_grid").append(cancel_link);
					$("#cancel").hide();

					let done_link = document.createElement('button');
					done_link.className = "sul-btn edit-btn btn-primary";
					done_link.innerHTML = "Done";
					done_link.id = "done";
					$(".edit_grid").append(done_link);
					$("#done").hide();

					var new_message = document.createElement("p");
					shuttle.newMsg = new_message;
					document.getElementById("edit").removeEventListener("click", editButtonClick);
					document.getElementById("edit").addEventListener("click", editButtonClick);

				}
			});
		});
	}

	reportEvents(comment_id, message) {
		/* report events 
		/* I dont know how not spam click events....
		*/

		$(`#${comment_id}_report`).on('click', function () {
			reportReview(comment_id, message);
		});
	}

}
function loadComments() {
	postRef.child("page_info").on("value", function (snapshot) {
		if (snapshot.val() != null) {
			display_rating = snapshot.val().rating;
		}
	});

	postRef.on("child_added", function (snapshot) {
		var child_name = snapshot.key;
		if (child_name != "page_info") {
			var newPost = snapshot.val();
			var comment = new Comments;

			comment.addTo("comment_div", snapshot.key, newPost.name, newPost.message, newPost.rating);
			comment.reportEvents(snapshot.key, newPost.message);
		}
	});
}

function edit_button() {
	var user = firebase.auth().currentUser;
	postRef.child(user.uid).once("value", function (snapshot) {
		if (snapshot.exists()) {
			let getGrid = new Comments;
			getGrid.editGrid(user.uid);
			getGrid.clickEvents();
			$("#comment_list").hide();
		}
	});
}
document.addEventListener("DOMContentLoaded", function () {
	firebase.auth().onAuthStateChanged(function (user) {
		if (user) {

			document.getElementById("comments").innerHTML = "Leave a review";

			if (!user.emailVerified) {
				document.getElementById("verifyEmailBanner").style.display = "";

				var height = document.getElementById("verifyEmailBanner").getBoundingClientRect().height;
				document.getElementById("placeholder").style.height = height + "px";
			} else {
				document.getElementById("comment_list").style.display = "";
			}
			function initReview() {
				var userRef = firebase.database().ref("users").child(user.uid);

				let reviewImg = document.getElementById("defaultImg");
				let reviewName = document.getElementById("defaultName");

				userRef.once('value', function (snapshot) {
					reviewName.innerHTML = snapshot.val().username;
				});
				firecloud.ref().child("pfp/" + user.uid).getDownloadURL()
					.then((url) => {
						reviewImg.src = url;
					})
					.catch((err) => {
						reviewImg.src = defaultImg;
					});
			}

			initReview();

			function getUsername() {
				var name;
				var user = firebase.auth().currentUser;
				var userRef = firebase.database().ref('users/' + user.uid);
				userRef.on("value", function (snapshot) {
					name = snapshot.val().username;
				});
				return name;
			}

			// basic functions init
			getUsername();

			edit_button();

			setFirst();

			loadComments();

			function review() {
				if (!navigator.onLine) {
					return mdcWarn("You cannot comment when you are offline");
				}
				document.getElementById("message").value = DOMPurify.sanitize(document.getElementById("message").value, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true })
				if (document.getElementById("message").value == "") return;
				var star_val = document.getElementById("comment").star.value;
				if (star_val == "") {
					return;
				}

				postRef.once("child_added", function (snapshot) {
					postRef.child(user.uid).update({
						message: $("#message").val(),
						name: getUsername(),
						rating: parseInt(star_val)
					}).catch(function (err) {
						if (err.message.indexOf("Permission") > 0) {
							mdcWarn("Please verify your email");
							firebase.auth().currentUser.sendEmailVerification();
							firebase.auth().signOut().then(function () {
								return (location.href = "/login");
							});
						}
					});
					if (snapshot.key == "page_info") {
						postRef.child(snapshot.key).update({
							rating: loadStars()
						});
					}
					document.getElementById("message").value = "";
				});
				var on_done = new Comments;
				on_done.editGrid(user.uid);
				on_done.clickEvents();

				// show recalc'ed page rating
				$("#displayRating").empty();
				pageDisplay(loadStars());

				// hide review box
				$("#comment_list").hide();
			}
			document.getElementById("comments").onclick = review;
		} else {

			loadComments();

			document.getElementById("signInBanner").style.display = "";

			var height = document.getElementById("signInBanner").getBoundingClientRect().height;
			document.getElementById("placeholder").style.height = height + "px";
		}
	});

	console.log("loaded");

});