const fSL = 1000000; //1 Mb

function init() {
  //init selectTab to the tab which the user clicked for. defaulted to login, but if we are going to add a btn that redirects straight to signup, then selectTab that instead
  selectTab(document.getElementById("profiletab"));
  $("#nav").prop("checked", false);
  for (var el of document.getElementsByClassName("entry")) {
    mdc.textField.MDCTextField.attachTo(el);
  }
  $("#new-password-field").hide();
  $("#confirm-password-field").hide();

  delWarnBox = new mdc.dialog.MDCDialog(document.querySelector('#deleteWarn'));
  tempImgJS = document.getElementById("tempImg");

  sAdjust();

  //NSFW Nudity - Literally you don't even need to steal this API Key just go to deepAI for a free one
  deepai.setApiKey("REDACTED");
}

function sAdjust() {
  pfpEdit = document.getElementById("pfpSpan");
  var coords = document.getElementById("pfp").getBoundingClientRect();
  pfpEdit.style.top = coords.top;
  pfpEdit.style.left = coords.left;
  pfpEdit.style.height = coords.height + "px";
  pfpEdit.style.width = coords.width + "px";
}

function renderReview(elementId, user_id, name, message, rating, permalink) {
  let container_div = document.createElement("div");
  container_div.className = "comment";

  var comment_div = document.createElement('div');
  name = DOMPurify.sanitize(name, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });
  message = DOMPurify.sanitize(message, { ALLOWED_TAGS: ["#text"], KEEP_CONTENT: true });


  var pfp_image = document.createElement("img");
  pfp_image.className = "profile_comment";

  const defaultImg = "/assets/images/default.PNG";

  firecloud.ref().child("pfp/" + user_id).getDownloadURL()
    .then((url) => {
      pfp_image.src = url;
    })
    .catch(() => {
      pfp_image.src = defaultImg;
    });


  container_div.append(pfp_image);

  var comment_name = document.createElement('h4');
  comment_name.innerHTML += name;

  comment_div.append(comment_name);


  var rating_para = document.createElement('span');

  var comment_rating = document.createElement('i');
  comment_rating.className = "fas fa-star";

  rating_para.append(rating.toString());
  rating_para.append(comment_rating);
  comment_div.append(rating_para);

  var comment_message = document.createElement('p');
  comment_message.innerHTML += message;
  comment_div.append(comment_message);

  var jumpLink = document.createElement("a");
  jumpLink.innerHTML = "Go To Page";
  jumpLink.href = permalink;
  jumpLink.className = "jump";
  comment_div.append(jumpLink);

  container_div.append(comment_div);

  document.getElementById(elementId).appendChild(container_div);
}
function selectTab(el) {
  for (let item of document.getElementsByClassName("dash-link")) {
    item.classList.remove("active");
    var tab = document.getElementById(item.id.slice(0, -3));
    tab.style.display = "none";
    for (item in tab.getElementsByTagName("input")) {
      //tab.getElementsByTagName("input")[item].required = false;
    }
  }

  el.classList.add("active");
  document.getElementById(el.id.slice(0, -3)).style.display = "block";
  var arr = document
    .getElementById(el.id.slice(0, -3))
    .getElementsByTagName("input");
  for (item in arr) {
    //arr[item].required = true;
  }
}

function selectImage(element) {
  for (let item of document.getElementsByClassName("grid_img")) {
    item.classList.remove("active_img");
  }

  element.classList.add("active_img");
}

function toggleMenu() {
  let pfpMenu = document.getElementById("pfpMenu");
  var display = pfpMenu.style.display;
  if(display == "grid") {
    pfpMenu.style.display = "";
    document.getElementsByClassName("image_box")[0].style.display="block";
  }
  else {
    pfpMenu.style.display = "grid";
    document.getElementsByClassName("image_box")[0].style.display="none";
  }
}


interestLazyloadShuttle = { num: 0, indexesRendered: [] };
function kickoff() {
  //kickoff == basically an "onload" handler, but for stuff that depends on firebase and interest list
  for (
    let i = interestLazyloadShuttle.num;
    i < interestLazyloadShuttle.num + 6;
    i++
  ) {
    let tempList = interestedClubs[i];
    if (tempList == undefined) return;
    let school = tempList.split("/")[0];
    let clubname = tempList.split("/")[1];
    db.collection("Schools")
      .where("name", "==", clubname)
      .where("school", "==", school)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          if (!interestLazyloadShuttle.indexesRendered.includes(doc.id)) {
            interestLazyloadShuttle.indexesRendered.push(doc.id);
            cardDisplay("container", doc, doc.data().school);
          }
        });
      });
  }
  interestLazyloadShuttle.num += 6;
}
document.addEventListener("scroll", function () {
  if (
    $(window).scrollTop() + window.innerHeight >=
    $(document).height() - 50
  ) {
    kickoff();
  }
});

document.addEventListener("DOMContentLoaded", function () {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      try {
        var userRef = firebase.database().ref("/users/").child(user.uid);
        var commentsRef = firebase.database().ref("/comments");
        var user_info = {
          firstname: null,
          lastname: null,
          username: null,
          pfpImg: null,
        };
        userRef.on("value", (snapshot) => {
          var info = snapshot.val();

          //update profile fields
          document.getElementById(
            "name"
          ).innerHTML = `${info.firstname} ${info.lastname}`;
          document.getElementById("username").innerHTML = info.username;
          document.getElementById("email").innerHTML = user.email;

          user_info.firstname = info.firstname;
          user_info.lastname = info.lastname;
          user_info.username = info.username;

          //Updates pfp
          let getPfp = new Promise((resolve, reject) => {
            try {
              firecloud.ref().child(info.pfpImg).getDownloadURL()
                .then((url) => {
                  user_info.pfpImg = url;
                  resolve();
                })
            } catch {
              user_info.pfpImg = "/assets/images/default.PNG";
              resolve();
            }
          });

          getPfp
            .then(() => {
              document.getElementById("pfp").src = user_info.pfpImg;
            })
            .catch(() => {
              console.log("Looks like smth happened");
            })

          //update comments
          commentsRef.on("child_added", (snapshot) => {
            var user_post = commentsRef.child(snapshot.key).child(user.uid);

            if (snapshot.hasChild(user.uid)) {
              user_post.once("value", () => {
                user_post.update({
                  name: info.username,
                });
              });
            }
          });
        });

        //assign onclick events to buttons. all functions are declared here to keep variables in the local scope
        document.getElementById("change-info").onclick = function () {
          changeInfo(
            document.getElementsByClassName("basic-box"),
            this,
            document.getElementById("confirm-info")
          );
        };

        document.getElementById("toggle-password").onclick = function () {
          togglePass("password", this);
        };
        document.getElementById("toggle-new-password").onclick = function () {
          togglePass("new-password", this);
        };
        document.getElementById("toggle-delete").onclick = function () {
          togglePass("delete-pw", this);
        };

        function togglePass(fieldid, eye) { //make visibility reset upon cancel/confirm
          if (![...eye.classList].includes("fa-eye-slash")) {
            document.getElementById(fieldid).type = "text";
            return eye.classList.add("fa-eye-slash");
          }
          document.getElementById(fieldid).type = "password";
          eye.classList.remove("fa-eye-slash");
        }

        function changeInfo(element, btn, confirmBtn) {
          //info fields
          var children = [];
          var input_fields = []; //make a var
          let changepw = false;
          for (let i = 0; i < element.length; i++) {
            children[i] = element[i].getElementsByClassName("user-info")[0];
            input_fields[i] = new InputField(children[i]);
          }

          for (let i = 0; i < element.length; i++) {
            element[i].prepend(input_fields[i].field);
            //element[i].removeChild(children[i]);
            children[i].style.display = "none";
          }

          //password fields
          document.getElementById("password-field-container").style.visibility = "visible"; //using visibility and height since changing display to none and back to block with the field already filled in causes the stupid chrome autocomplete highlighting
          document.getElementById("password-field-container").style.height = "initial";
          document.getElementById("change-password").style.display = "block";
          document.getElementById("change-password").onclick = function () {
            document.getElementById("new-password-field").style.display = "block";
            document.getElementById("confirm-password-field").style.display = "block";
            document.getElementById("change-password").style.display = "none";
            changepw = true;
          }

          //buttons
          confirmBtn.style.visibility = "visible";
          confirmBtn.onclick = () => {
            confirm(changepw, input_fields, function () {
              cancelInfo(element, btn, confirmBtn, input_fields);
            });
          };
          btn.innerHTML = "Cancel";
          btn.onclick = () => {
            cancelInfo(element, btn, confirmBtn, input_fields);
          };
        }

        function cancelInfo(element, btn, confirmBtn, input_fields) {
          //info fields
          for (let i = 0; i < element.length; i++) {
            element[i].removeChild(input_fields[i].field);
            input_fields[i].span.style.display = "flex";
          }

          //password fields
          document.getElementById("password-field-container").style.visibility = "hidden";
          document.getElementById("password-field-container").style.height = "0";
          document.getElementById("new-password-field").style.display = "none";
          document.getElementById("confirm-password-field").style.display = "none";

          //buttons
          confirmBtn.style.visibility = "hidden";
          btn.innerHTML = "Edit";
          btn.onclick = () => {
            changeInfo(element, btn, confirmBtn);
          };
        }

        function confirm(changepw, input_fields, closefields) {
          let infochanged = false;
          var info = input_fields.map((item) =>
            DOMPurify.sanitize(item.input.value, {
              ALLOWED_TAGS: [],
              KEEP_CONTENT: true,
            })
          );

          const name = info[0],
            username = info[1],
            email = info[2],
            current_pw = document.getElementById("password").value;

          if (!current_pw) return mdcWarn("Please enter your password");
          var credential = firebase.auth.EmailAuthProvider.credential(
            user.email,
            current_pw
          );
          user
            .reauthenticateWithCredential(credential)
            .then(() => {
              if (`${user_info.firstname} ${user_info.lastname}` != name) {
                //check
                let x = name.split(" ");
                for (item in x)
                  if (x[item] == "") x.splice(item); //remove trailing spaces
                if (x.length != 2)
                  return mdcWarn(
                    "Name is filled out incorrectly.\nYou must include only your first and last name separated by a space"
                  );

                //write db
                userRef.update({
                  firstname: capitalize(x[0]),
                  lastname: capitalize(x[1]),
                });
                infochanged = true;
              }
              if (user_info.username != username) {
                //check
                if (!/^[a-zA-Z0-9-_].{3,12}$/.test(username))
                  return mdcWarn(
                    "Invalid username.\nYour name may only contain letters, numbers, dashes and underscores.\nAnd it must be between 3-12 characters"
                  );
                //write db
                userRef.update({
                  username: username,
                });
                infochanged = true;
              }
              if (user.email != email) {
                //check
                if (
                  !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
                )
                  return mdcWarn("You have entered an invalid email");
                //update auth
                firebase
                  .auth()
                  .currentUser.verifyBeforeUpdateEmail(email)
                  .then(() => {
                    mdcWarn("Verification email sent to new email");
                  })
                  .catch((error) => {
                    mdcWarn(error); //update error
                    console.log(error);
                  });

                infochanged = true;
              }
              if (!changepw) {
                if (infochanged) {
                  document.getElementById("password").value = "";
                }
                closefields();
              }
            })
            .catch((error) => {
              mdcWarn(error.message); //reauthentication error
              console.log(error);
            });

          if (changepw) {
            const new_pw = document.getElementById("new-password").value,
              confirm_pw = document.getElementById("confirm-new-password").value

            //check
            if (new_pw !== confirm_pw)
              return mdcWarn("Your passwords do not match");
            if (!/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,32}$/.test(new_pw))
              return mdcWarn(
                "Your password must be 6 characters long and contain at least:\nOne uppercase letter\nOne lowercase letter\nOne number"
              );
            //update auth
            var credential = firebase.auth.EmailAuthProvider.credential(
              user.email,
              current_pw
            );
            user
              .reauthenticateWithCredential(credential)
              .then(() => {
                user
                  .updatePassword(new_pw)
                  .then(() => {
                    mdcWarn("You have updated your password");

                    document.getElementById("password").value = "";
                    document.getElementById("new-password").value = "";
                    document.getElementById("confirm-new-password").value = "";

                    closefields();
                  })
                  .catch((error) => {
                    console.log(error); //update error
                  });
              })
              .catch((error) => {
                //reauthentication error
                if (error.code == "auth/wrong-password") {
                  mdcWarn("Your password is incorrect");
                }
              });
          }
        }

        function getUsername() {
          var name;
          var user = firebase.auth().currentUser;
          var userRef = firebase.database().ref("users/" + user.uid);
          userRef.on("value", function (snapshot) {
            name = snapshot.val().username;
          });
          return name;
        }
        getUsername();

        function escapeHtml(str) {
          return "<span>" + str + "</span>";
        }

        function loadReviews(uid) {
          var user_id = uid;
          var commentsRef = firebase.database().ref("/comments");
          commentsRef.on("child_added", function (snapshot) {
            post = snapshot.key;
            var user_post = commentsRef.child(post).child(user_id);

            if (snapshot.hasChild(user_id)) {
              var permalink = commentsRef.child(post).child("page_info");
              var perma;
              var name;
              permalink.on("value", function (snap) {
                perma = snap.val().permalink;
                name = snap.val().name;
              });

              user_post
                .orderByKey()
                .equalTo(uid)
                .once("value", function () {
                  user_post.on("value", function (data) {
                    //! Recreate
                    return renderReview("appendTo", user_id, name, data.val().message, data.val().rating, perma)
                  });
                });
            }
          });
        }
        loadReviews(user.uid);
      } catch (err) { }
    } else {
      location.href = "/login";
    }
  });
});

function InputField(child) {
  this.span = child;

  this.field = document.createElement("div");
  this.field.className =
    "mdc-text-field mdc-text-field--outlined mdc-text-field--label-floating user-field";
  this.field.id = child.id + "-field";

  this.input = document.createElement("input");
  this.input.id = child.id + "-input"; //password-input, email-input, etc...
  this.input.type = child.id;
  this.input.value = child.innerHTML;
  if (child.id == "name") {
    this.input.onkeypress = () => {
      return /[a-z]|\s/i.test(event.key); //regular expression
    };
  }
  if (child.id == "username") {
    this.input.onkeypress = () => {
      return /^[a-zA-Z0-9-_]+$/.test(event.key);
    };
  }
  this.input.className = "mdc-text-field__input";

  var div1 = document.createElement("div");
  div1.className = "mdc-notched-outline";

  var div2 = document.createElement("div");
  div2.className = "mdc-notched-outline__leading";

  var div3 = document.createElement("div");
  div3.className = "mdc-notched-outline__notch";

  var div4 = document.createElement("div");
  div4.className = "mdc-notched-outline__trailing";

  div1.appendChild(div2);
  div1.appendChild(div3);
  div1.appendChild(div4);

  this.field.appendChild(this.input);
  this.field.appendChild(div1);
}
function capitalize(str) {
  return str.substr(0, 1).toUpperCase() + str.substr(1, str.length - 1);
}

function checkNsfw(e) {
  return new Promise((resolve, reject) => {
    var result = deepai.callStandardApi("content-moderation", {
      image: e
    }).then((result) => {
      resolve(result.output);
    }).catch((err) => {
      console.log("Error in content moderation");
      reject(err);
    })
  })
}

function changePfp(e) {
  var uid = firebase.auth().currentUser.uid;
  var sRef = firecloud.ref().child("pfp/" + uid);

  //Checks file size
  if (e.files[0].size > fSL) {
    mdcWarn("Your file size is too big. Try another photo.");
    return;
  }

  checkNsfw(e).then((output) => {
    if (output.nsfw_score > 0.15) {
      if (output.detections.length != 0) {
        let detection = "";
        for (var x in output.detections) {
          detection += output.detections[x].name;
          if (x != 0) {
            detection += ", ";
          }
        }
        mdcWarn(`Your photo is inappropriate and contains the following: ${detection}. Please use another photo`);
      }
      else {
        mdcWarn("Your photo is not safe for work. Please use another photo.");
      }
    }
    else {
      try {
        firebase.database().ref().child(`users/${uid}`).update({
          pfpImg: sRef.location.path
        });
        sRef.put(e.files[0]).then(() => {
          mdcWarn("PFP Updated. Please keep in mind we reserve the right to remove your PFP is it is deemed offensive.");
          location.reload();
        })
      }
      catch (err) {
        console.log(err);
      }
    }
  });
}

//Onclick for img serves as a backup in case the span fails
function pfpSpanAdjust(isHover) {
  if (isHover) {
    pfpEdit.style.display = "flex";
  }
  else {
    pfpEdit.style.display = "none";
  }
}

function delInit(delBtn) {
  let password = document.getElementById("delete-pw").value;
  let email = document.getElementById("delete-email").value;
  let checkEmail = document.getElementById("delete-pw").checkValidity();
  let checkPass = document.getElementById("delete-email").checkValidity();

  if ((password.length < 4 || email.length < 5) || (checkEmail == false || checkPass == false)) return
  delWarnBox.open();
}

function removeAcc(event) {
  //Disables any movement
  document.body.style.pointerEvents = "none"
  document.body.style.cursor = "wait";

  var user = firebase.auth().currentUser;
  let password = document.getElementById("delete-pw").value;
  let email = document.getElementById("delete-email").value;

  if (password.length < 4 || email.length < 5) return
  var credential = firebase.auth.EmailAuthProvider.credential(email, password);
  user.reauthenticateWithCredential(credential).then(() => {
    console.log("reauth was a success!")
  }).then(() => {
    var commentsRef = firebase.database().ref("/comments");
    commentsRef.once("value", (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        if (childSnapshot.hasChild(user.uid)) {
          //console.log(childSnapshot.key + "/" + user.uid);
          commentsRef.child(childSnapshot.key + "/" + user.uid).remove()
            .catch((err) => {
              console.log(err);
            });
        }
      })
    })

    //If pfp exists, delete it. If not, ignore
    firecloud.ref("pfp/" + user.uid).delete()
      .catch((err) => {
        console.log(err);
      });

    firebase.database().ref().child("users/" + user.uid).remove()
      .then(() => {
        console.log("deleted!");
      })
      .catch((err) => {
        console.log(err);
      })

    user.delete()
      .then(() => {
        logOut(event);
        location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  });
}