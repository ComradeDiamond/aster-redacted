document.addEventListener("DOMContentLoaded", function(){
    if (navigator.onLine) {
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                for (var el of document.getElementsByClassName("accountBtn")) {
                    el.style.display = "";
                }
                var userRef = firebase.database().ref('/users/').child(user.uid)
                userRef.on("value", function(snapshot){
                    document.getElementById("userBtn").innerHTML = DOMPurify.sanitize(snapshot.val().username, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
                    document.getElementById("drawerUsername").innerHTML = "Hey, " + DOMPurify.sanitize(snapshot.val().username, {ALLOWED_TAGS: [], KEEP_CONTENT: true});
                    document.getElementById("ibc").style.display = "";
                    
                    if(snapshot.val().role !== "manager") {
                        location.href = '/dashboard';
                        document.getElementById("managerLink").style.display = "none";
                        document.getElementById("manageBtn").style.display = "none";
                    }
                    loadOut();
                });
                document.getElementById("loginBtn").style.display = "none";
                document.getElementById("userBtn").addEventListener("click", function(e) {
                    e.preventDefault();
                    toggleDropDown(e);
                });
            } else {
                loadOut();
            }
        });
    } else {
        loadOut();
    }
});
function logOut(e){
    e.preventDefault();
    firebase.auth().signOut();
    if(window.location.pathname == "/dashboard"){
        location.href = "/login";
    }
    else{
        location.reload();
    }
}
function loadOut(){
    document.getElementById("loading-screen").classList.add("fade");
    setTimeout(function(){
        document.getElementById("loading-screen").style.display = "none";
        document.getElementById("loading-flower").classList.remove("loading-spin");
    }, 500);
}
function toggleDropDown(e){
    var menu = document.getElementById("dropdownMenu");
    if(menu.style.display == "none" && e.target == document.getElementById("userBtn"))
        return menu.style.display = "block";
        
    menu.style.display = "none";
}
window.addEventListener("click", (e) => {
    if(e.target != document.getElementById("userBtn"))
    toggleDropDown(e);
});
/*
window.addEventListener("error", (e) => {
    loadOut();
}, true);*/