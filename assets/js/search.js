
class Filter {
    byName(input, days) {

        let club_name = input.toLowerCase();

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            db.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                        indexesRendered.push(doc.id);
                        console.log(doc.id, " => ", doc.data().keywords);
                        cardDisplay("searchResults", doc, doc.data().school);
                        tempHasRendered++;
                    }
                    availableOnFirestore++;
                });
            }).then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });

        }

        function helper() {
            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    byCategory(array, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            db.collection("Schools").where('category', 'array-contains-any', array).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                        indexesRendered.push(doc.id);
                        console.log(doc.id, " => ", doc.data().keywords);
                        cardDisplay("searchResults", doc, doc.data().school);
                        tempHasRendered++;
                    }
                    availableOnFirestore++;
                });
            }).then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    byNameAndCategory(name, category_array, days) {

        let dataTransport = {};
        let lastDoc;
        let club_name = name.toLowerCase();
        let indexesRendered = [];
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            db.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {

                doc.forEach(function (snapshot) {
                    category_array.forEach((category) => {
                        if (snapshot.data().category.includes(category) && !indexesRendered.includes(snapshot.id) && (days.length == 0 || meetsOnDay(snapshot, days))) {
                            indexesRendered.push(snapshot.id);
                            cardDisplay("searchResults", snapshot, snapshot.data().school);
                            tempHasRendered++;
                        }
                        availableOnFirestore++;
                    });
                })

            }).then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    bySchool(school_array, days) {

        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];
        let schoolCount = 0;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            let getData = new Promise(function (resolve, reject) {
                school_array.forEach(async (school) => {
                    await db.collection("Schools").where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                cardDisplay("searchResults", doc, doc.data().school);
                                tempHasRendered++;
                            }
                            availableOnFirestore++;
                        });
                    });
                    schoolCount++;
                    if (schoolCount == school_array.length) resolve();
                });
            });
            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });
        }


        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    bySchoolAndName(school_array, name, days) {

        const club_name = name.toLowerCase();
        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let schoolCounter = 0;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            let getData = new Promise(function (resolve, reject) {
                school_array.forEach(async (school) => {
                    await db.collection("Schools").where('school', '==', school).where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {
                        doc.forEach(function (snapDoc) {
                            if (!indexesRendered.includes(snapDoc.id) && (days.length == 0 || meetsOnDay(snapDoc, days))) {
                                indexesRendered.push(snapDoc.id);
                                cardDisplay("searchResults", snapDoc, snapDoc.data().school);
                                tempHasRendered++;
                            }
                            availableOnFirestore++;
                        });
                    });
                    schoolCounter++;
                    if (schoolCounter == school_array.length) resolve();
                });
            });
            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });
        }


        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    bySchoolAndCategory(school_array, category_array, days) {
        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];
        let schoolCount = 0;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            let getData = new Promise(function (resolve, reject) {
                school_array.forEach(async (school) => {
                    await db.collection("Schools").where('category', 'array-contains-any', category_array).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (doc) {

                        doc.forEach(function (snapDoc) {
                            if (!indexesRendered.includes(snapDoc.id) && (days.length == 0 || meetsOnDay(snapDoc, days))) {
                                indexesRendered.push(snapDoc.id);
                                cardDisplay("searchResults", snapDoc, snapDoc.data().school);
                                tempHasRendered++;
                            }
                            availableOnFirestore++;
                        });
                        schoolCount++;
                        if (schoolCount == school_array.length) resolve();
                    });
                });
            });
            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }
                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });
        }



        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);

    }

    bySchoolNameCategory(school_array, category_array, name, days) {

        const club = name.toLowerCase();
        let dataTransport = {};
        let lastDoc;
        let indexesRendered = [];
        let schoolCount = 0;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;

            let getData = new Promise(function (resolve, reject) {
                school_array.forEach(async (school) => {
                    await db.collection("Schools").where('keywords', 'array-contains-any', [club]).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            category_array.forEach((category) => {
                                if (doc.data().category.includes(category) && !indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            })
                        })
                    });
                    schoolCount++;
                    if (schoolCount == school_array.length) resolve();
                });
            })
            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }
                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;
            });


        }


        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }



    //interest
    byCategoryAndInterest(array, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    await db.collection("Schools").where('category', 'array-contains-any', array).where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                cardDisplay("searchResults", doc, doc.data().school);
                                tempHasRendered++;
                            }
                            availableOnFirestore++;
                        });
                    });
                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    //name
    /*
        take searchbar string and check substring with each club name in interestedClubs
    */
    byNameAndInterest(query, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        query = query.toLowerCase();

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (name.indexOf(query) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    //school
    /*
        take each club's school in interestedClubs and check if it's one of the checked schools
    */
    bySchoolAndInterest(school_array, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (school_array.indexOf(school) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    //name+school
    /*
        both
    */
    bySchoolAndNameAndInterest(school_array, query, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        query = query.toLowerCase();

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (school_array.indexOf(school) > -1 && name.indexOf(query) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    //name+category
    /*
        name thing, then
        do array-contains-any for categories
    */
    byNameAndCategoryAndInterest(query, categories, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        query = query.toLowerCase();

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (name.indexOf(query) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('category', 'array-contains-any', categories).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

    //category+school
    /*
        school thing, then
        do array-contains-any for categories
    */
    byCategoryAndSchoolAndInterest(school_array, categories, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (school_array.indexOf(school) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('category', 'array-contains-any', categories).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdCWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }


    //name+category+school
    /*
        name and school thing, then
        do array-contains-any for categores
    */
    byNameAndCategoryAndSchoolAndInterest(school_array, categories, query, days) {

        let dataTransport = {};
        let indexesRendered = [];
        let lastDoc;
        let availableOnFirestore = 0;
        let tempHasRendered = 0;
        let processed = 0;

        query = query.toLowerCase();

        var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
        if (toRemove != undefined) { //prevents error from removing undefined
            window.removeEventListener("scroll", toRemove); //remove old
        }

        function search(lastIndex, limit) {
            availableOnFirestore = 0;
            tempHasRendered = 0;
            processed = 0;

            let getData = new Promise(function (resolve, reject) {
                let subarray = interestedClubs.splice(lastIndex, lastIndex + limit);
                subarray.forEach(async function (club) {
                    let splitted = club.split("/");

                    let school = splitted[0];
                    let name = splitted[1];

                    processed++;

                    if (school_array.indexOf(school) > -1 && name.indexOf(query) > -1) {
                        await db.collection("Schools").where('school', '==', school).where('category', 'array-contains-any', categories).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
                            querySnapshot.forEach(function (doc) {
                                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                    indexesRendered.push(doc.id);
                                    console.log(doc.id, " => ", doc.data().keywords);
                                    cardDisplay("searchResults", doc, doc.data().school);
                                    tempHasRendered++;
                                }
                                availableOnFirestore++;
                            });
                        });
                    }

                    if (processed == subarray.length) resolve();
                });
            });

            getData.then(function () {
                lastIndex = parseFloat(lastIndex) + limit;
                lastIndex = lastIndex.toString();
                dataTransport.lastIndex = lastIndex;

                if (availableOnFirestore > 0 && tempHasRendered == 0) {
                    lastDoc = dataTransport.lastIndex;
                    lastDoc = parseFloat(lastDoc) + 1;
                    return search(lastDoc.toString(), 6);
                } else if (indexesRendered.length == 0) {
                    mdcWarn("Try a different query");
                    var toRemove = document.getElementsByClassName("applyButton")[0].handler; //obtain the reference to old
                    if (toRemove != undefined) { //prevents error from removing undefined
                        window.removeEventListener("scroll", toRemove); //remove old
                    }
                    return;
                }

                window.addEventListener("scroll", helper); //add new
                document.getElementsByClassName("applyButton")[0].handler = helper;

            });
        }

        function helper() {

            lastDoc = dataTransport.lastIndex;
            let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
            if (scrollAtBottom) {

                lastDoc = parseFloat(lastDoc) + 1;
                search(lastDoc.toString(), 4);

            }
        }
        search("0", 6);
    }

}

/**
 * Returns a boolean based on whether the club meets on the requested day
 * @param {FirestoreDocument} clubData Club's data from Firestore
 * @param {Array} days Array of days checked by user
 * @returns {Boolean} if one of the days in the days is met by club
 */
function meetsOnDay(clubData, days) {
    for (var day of days) {
        if ((clubData.data().meets.indexOf(day) > -1)) {
            return true;
        }
    }
    return false;
}