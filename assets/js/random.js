class RandomFilter {
    byName(input, days) {

        let club_name = input.toLowerCase();

        let indexesRendered = [];
        let count = [];
        db.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                    indexesRendered.push(doc.id);
                    count.push(doc.data().link);
                }
            });
        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    byCategory(array, days) {

        let indexesRendered = [];
        let count = [];

        db.collection("Schools").where('category', 'array-contains-any', array).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                    indexesRendered.push(doc.id);
                    count.push(doc.data().link);
                }
            });
        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });

    }


    byNameAndCategory(name, category_array, days) {

        let club_name = name.toLowerCase();
        let indexesRendered = [];
        let count = [];

        db.collection("Schools").where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (doc) {

            doc.forEach(function (snapshot) {
                category_array.forEach((category) => {
                    if (snapshot.data().category.includes(category) && !indexesRendered.includes(snapshot.id) && (days.length == 0 || meetsOnDay(snapshot, days))) {
                        indexesRendered.push(snapshot.id);
                        count.push(snapshot.data().link);
                    }
                });
            })

        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }


    bySchool(school_array, days) {

        let indexesRendered = [];
        let count = [];
        let school = school_array[Math.floor(Math.random() * school_array.length)];

        db.collection("Schools").where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                    indexesRendered.push(doc.id);
                    count.push(doc.data().link);
                }
            });
        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    bySchoolAndName(school_array, name, days) {

        const club_name = name.toLowerCase();
        let count = [];
        let indexesRendered = [];
        let school = school_array[Math.floor(Math.random() * school_array.length)];

        db.collection("Schools").where('school', '==', school).where('keywords', 'array-contains-any', [club_name]).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (doc) {
            doc.forEach(function (snapDoc) {
                if (!indexesRendered.includes(snapDoc.id) && (days.length == 0 || meetsOnDay(snapDoc, days))) {
                    indexesRendered.push(snapDoc.id);
                    count.push(snapDoc.data().link);
                }
            });
        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    bySchoolAndCategory(school_array, category_array, days) {

        let indexesRendered = [];
        let count = [];
        let school = school_array[Math.floor(Math.random() * school_array.length)];

        db.collection("Schools").where('category', 'array-contains-any', category_array).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (doc) {
            doc.forEach(function (snapDoc) {
                if (!indexesRendered.includes(snapDoc.id) && (days.length == 0 || meetsOnDay(snapDoc, days))) {
                    indexesRendered.push(snapDoc.id);
                    count.push(snapDoc.data().link);
                }
            });
        }).then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    bySchoolNameCategory(school_array, category_array, name, days) {

        const club = name.toLowerCase();
        let count = [];
        let indexesRendered = [];
        let school = school_array[Math.floor(Math.random() * school_array.length)];

        db.collection("Schools").where('keywords', 'array-contains-any', [club]).where('school', '==', school).orderBy(firebase.firestore.FieldPath.documentId()).startAt("0").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                category_array.forEach((category) => {
                    if (doc.data().category.includes(category) && !indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                        indexesRendered.push(doc.id);
                        count.push(doc.data().link);
                    }
                })
            })
        }).then(function () {
            if (count == 0) return mdcWarn("Try another query");
            var random = Math.floor(Math.random() * count.length);
            location.href = count[random];
        });
    }

    byCategoryAndInterest(array, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                processed++;

                await db.collection("Schools").where('category', 'array-contains-any', array).where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                            indexesRendered.push(doc.id);
                            console.log(doc.id, " => ", doc.data().keywords);
                            count.push(snapDoc.data().link);
                        }
                    });
                });
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }


    //name
    /*
        take searchbar string and check substring with each club name in interestedClubs
    */
    byNameAndInterest(query, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (name.indexOf(query) > -1) {
                    await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    //school
    /*
        take each club's school in interestedClubs and check if it's one of the checked schools
    */
    bySchoolAndInterest(school_array, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (school_array.indexOf(school) > -1) {
                    await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    //name+school
    /*
        both
    */
    bySchoolAndNameAndInterest(school_array, query, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (name.indexOf(query) > -1 && school_array.indexOf(school) > -1) {
                    await db.collection("Schools").where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    //name+category
    /*
        name thing, then
        do array-contains-any for categories
    */
    byNameAndCategoryAndInterest(query, categories, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (name.indexOf(query) > -1) {
                    await db.collection("Schools").where('category', 'array-contains-any', categories).where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

    //category+school
    /*
        school thing, then
        do array-contains-any for categories
    */
    byCategoryAndSchoolAndInterest(school_array, categories, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (school_array.indexOf(school) > -1) {
                    await db.collection("Schools").where('category', 'array-contains-any', categories).where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });

    }


    //name+category+school
    /*
        name and school thing, then
        do array-contains-any for categores
    */
    byNameAndCategoryAndSchoolAndInterest(school_array, categories, query, days) {

        let indexesRendered = [];
        let count = [];
        let processed = 0;

        let getData = new Promise(function (resolve, reject) {
            let subarray = interestedClubs;
            subarray.forEach(async function (club) {
                let splitted = club.split("/");

                let school = splitted[0];
                let name = splitted[1];

                if (name.indexOf(query) > -1 && school_array.indexOf(school) > -1) {
                    await db.collection("Schools").where('category', 'array-contains-any', categories).where('school', '==', school).where('name', '==', name).orderBy(firebase.firestore.FieldPath.documentId()).startAt(0).limit(1).get().then(function (querySnapshot) {
                        querySnapshot.forEach(function (doc) {
                            if (!indexesRendered.includes(doc.id) && (days.length == 0 || meetsOnDay(doc, days))) {
                                indexesRendered.push(doc.id);
                                console.log(doc.id, " => ", doc.data().keywords);
                                count.push(snapDoc.data().link);
                            }
                        });
                    });
                }
                if (processed == subarray.length) resolve();
            });
        });

        getData.then(function () {
            if (count.length == 0) return mdcWarn("Try another query");
            location.href = count[Math.floor(Math.random() * count.length)];
        });
    }

}

/**
 * Returns a boolean based on whether the club meets on the requested day
 * @param {FirestoreDocument} clubData Club's data from Firestore
 * @param {Array} days Array of days checked by user
 * @returns {Boolean} if one of the days in the days is met by club
 */
function meetsOnDay(clubData, days) {
    for (var day of days) {
        if ((clubData.data().meets.indexOf(day) > -1)) {
            return true;
        }
    }
    return false;
}