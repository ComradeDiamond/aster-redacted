let indexTransport = {};
let indexesRendered = [];
function lazyload(elementId, lastIndex, limit) {
	db.collection("Schools").orderBy(firebase.firestore.FieldPath.documentId()).startAt(lastIndex).limit(limit).get().then(function (querySnapshot) {
		querySnapshot.forEach(function (doc) {
			if (!indexesRendered.includes(doc.id)) {
				console.log(doc.id, " => ", doc.data().name);
				cardDisplay(elementId, doc, doc.data().school);
			}
			indexesRendered.push(doc.id);
		});
	});
	lastIndex = parseFloat(lastIndex) + 1;
	lastIndex = lastIndex.toString();
	indexTransport.lastIndex = lastIndex;
	indexTransport.lastIndex = parseFloat(indexTransport.lastIndex) + 1;
}

window.addEventListener("scroll", lazyloadHelper);

function lazyloadHelper() {
	let lastDoc = indexTransport.lastIndex;
	let scrollAtBottom = $(window).scrollTop() + window.innerHeight >= $(document).height() - 50;
	if (scrollAtBottom && lastDoc < 9) {

		latDoc = parseFloat(lastDoc) + 1;
		lazyload("container", lastDoc.toString(), 4);

	}
}
