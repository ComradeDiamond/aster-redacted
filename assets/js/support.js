document.addEventListener("DOMContentLoaded", function () {
    for (var el of document.getElementsByClassName("entry")) {
        mdc.textField.MDCTextField.attachTo(el);
    }
});


function asdf(e) {
    e.preventDefault();

    var form = document.getElementById("authform");

    if (!form.checkValidity()) return;

    var email = form.email.value;
    var subject = form.subject.value;
    var message = form.msg.value;

    firestore.collection("Support").doc(email + " - " + subject).set({
        "email": email,
        "subject": subject,
        "message": message
    }).then(function () {
        mdcWarn("Thank you for contacting Aster.  Our team will follow up via email as neccessary.");
        location.reload();
    }).catch(function (error) {
        mdcWarn("Something went wrong.");
    });

}